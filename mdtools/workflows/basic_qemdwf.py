# -*- coding: utf-8 -*-
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"
import sys, copy, numpy as np
from math import ceil

from create_rescale import create_diamond_fcc, rescale

from aiida.workflows2.fragmented_wf import FragmentedWorkfunction, \
    ResultToContext, while_, if_

from aiida.common.constants import timeau_to_sec

from aiida.orm import CalculationFactory, load_node
from aiida.orm.data.simple import Float, Str, NumericType, SimpleData, make_str
from aiida.orm.data import Data
from aiida.orm.code import Code
from aiida.orm.data.parameter import ParameterData
from aiida.orm.data.structure import StructureData
from aiida.orm.data.upf import UpfData
from aiida.orm.data.remote import RemoteData
from aiida.orm.data.array.kpoints import KpointsData


from aiida.orm.group import Group
from aiida.orm.querybuilder import QueryBuilder

from aiida.orm.calculation.job.quantumespresso.pw import PwCalculation

from aiida.common.exceptions import InputValidationError
from aiida.workflows2.run import asyncd

ION_TEMP_CONTROL = 'rescaling'    

class BasicQEMDWF(FragmentedWorkfunction):
    _default_distance_kpoints_in_mesh = 0.2
    _default_offset_kpoints_mesh = [0., 0., 0.]
    _default_force_parity_kpoints_mesh = False
    _default_use_gamma_only = True

    @classmethod
    def _define(cls, spec):
        super(BasicQEMDWF, cls)._define(spec)
        spec.input("mdparams", valid_type=ParameterData)
        spec.input("resources", valid_type=ParameterData)
        spec.input("structure", valid_type=StructureData)
        spec.input("pwinputparams", valid_type=ParameterData)
        spec.input("pwsettings", valid_type=ParameterData)
        spec.input("pseudo_family_name", valid_type=Str)
        spec.input("plugin_name_scf", valid_type=Str, required=False, default=make_str('quantumespresso.pw'))
        spec.input("plugin_name_md", valid_type=Str, required=False, default=make_str('quantumespresso.pw'))
        spec.input("code_name_scf", valid_type=Str)
        spec.input("code_name_md", valid_type=Str)
        spec.outline(
            cls._initialize,
            cls._step_make_supercell,
            cls._step_start_w_scf,
            cls._step_adjust_parameters_2_NVT,
            while_(
                cls._is_not_thermalized
            )(
                cls._step_NVT,
                cls._step_analyze_NVT,
            ),
            cls._step_adjust_parameters_NVT_2_NVE,
            while_(
                cls._is_not_finished
            )(
                #~ if_(cls._is_not_on_hold)(cls._step_NVE)
                cls._step_NVE
            ),
            cls._step_results,
        )
        spec.dynamic_output()


    def _initialize(self, ctx):
        """
        Initializing the workflow and checking if inputs are valid.
        """
        ctx.calc_params = copy.deepcopy(self.inputs.pwinputparams.get_dict())
        # modify the calc params, make sure that cards CONTROL, ELECTRONS and SYSTEM exist:
        for cardn in ('CONTROL', 'ELECTRONS', 'SYSTEM'):
            ctx.calc_params[cardn] = ctx.calc_params.get(cardn, {})
        # If the card IONS is present, remove it:
        if 'IONS' in ctx.calc_params:
            raise InputValidationError("You can't specify IONS as a card in pwinputparams")


        # get the resources:
        try:
            ctx.max_walltime_seconds = int(self.inputs.resources.dict.max_walltime_seconds)
            ctx.max_seconds = ctx.max_walltime_seconds - 600
        except AttributeError:
            raise InputValidationError(
                "\nYou have to provide max_walltime_seconds in resources\n"
            )
        try:
            ctx.num_machines = int(self.inputs.resources.dict.num_machines)
        except AttributeError:
            raise InputValidationError(
                "\nYou have to provide num_machines in resources\n"
            )


        # get the mdparams:
        mdparams = self.inputs.mdparams.get_dict()
        # required:
        ctx.target_temperature_K = float(mdparams['target_temperature_K'])
        ctx.temperature_tolerance = float(mdparams.get('temperature_tolerance', 0.1))
        ctx.time_to_thermalize_fs = float(mdparams['thermalization_time_fs'])
        ctx.timestep_au_ry = float(mdparams['timestep_au_ry'])

        ctx.timestep_in_fs = 2*timeau_to_sec*1.0e15*ctx.timestep_au_ry

        ctx.minimal_dimension = float(mdparams['minimal_dimension'])
        ctx.rattle_sigma = mdparams.get('rattle_sigma', None)
        ctx.msd_std_threshold = self.inputs.mdparams.dict.msd_std_threshold
        #not required and None if not given
        if ctx.rattle_sigma is not None:
            ctx.rattle_sigma = float(ctx.rattle_sigma)
        ctx.max_nstep = mdparams.get('max_nstep', None)
        if ctx.max_nstep is not None:
            ctx.max_nstep = int(ctx.max_nstep)
        # not required with defaults given here:
        ctx.vaf_is_0_fs = float(mdparams.get('vaf_is_0_fs', 500.))
        ctx.block_length_fs = float(mdparams.get('block_length_fs', 5000.))
        ctx.max_t_msd_fs=float(mdparams.get('max_t_msd_fs', 10000.))
        ctx.species_of_interest = mdparams.get('species_of_interest', ['Li'])
        ctx.conductivity_rel_err_threshold = float(mdparams.get('conductivity_rel_err_threshold', 0.3)) # TODO additional check

        # Here I start the seq of calculations I launched (their uuid)
        ctx.calculation_seq = []
        
        # The code name, is this needed?
        ctx.code_name_scf = str(self.inputs.code_name_scf)
        ctx.code_name_md = str(self.inputs.code_name_md)
        
        # Here I store the iterations I did
        ctx.iterations = 0
        ctx.calculation_seq_nvt = []
        ctx.calculation_seq_nve = []
        # so that he will get into the loop:
        ctx.conductivity_rel_err = -1

    def _step_make_supercell(self, ctx):
        from mdtools.libs.struclib.inlinecalcs import make_supercell_inline
        ctx.supercell = make_supercell_inline(
                structure=self.inputs.structure,
                parameters=self.inputs.mdparams
            )[1]['supercell']


    def _step_start_w_scf(self, ctx):
        """
        In first step I am launching a PW-scf calculation.
        """
        print self._calc.pk, self._calc.uuid, "at _step_start_w_scf"

        proc = CalculationFactory(str(self.inputs.plugin_name_scf)).process()
        inputs = proc.get_inputs_template()

        # KPOINTS:
        if self.inputs.pwsettings.get_dict().get(
                'use_kpoints_gamma',
                self._default_use_gamma_only
            ):
            inputs.settings = ParameterData(dict=dict(gamma_only=True))
            inputs.kpoints = KpointsData()
            inputs.kpoints.set_kpoints_mesh([1,1,1])
        else:
            distance_kpoints = self.inputs.pwsettings.get_dict().get(
                                'distance_kpoints_in_mesh',
                                self._default_distance_kpoints_in_mesh
                            )
            offset = self.inputs.pwsettings.get_dict().get(
                    'offset_kpoints_mesh',
                    self._default_offset_kpoints_mesh
                )
            force_parity = self.inputs.pwsettings.get_dict().get(
                    'force_parity_kpoints_mesh',
                    self._default_force_parity_kpoints_mesh
                )
            kpoints = KpointsData()
            kpoints.set_cell_from_structure(self.inputs.structure)

            kpoints.set_kpoints_mesh_from_density(
                    distance_kpoints,
                    offset=offset,
                    force_parity=force_parity
                )
            inputs.kpoints=kpoints

        inputs.code = Code.get_from_string(ctx.code_name_scf)

        pseudo_dict = self.get_pseudos()
        ecutwfc, ecutrho = self.get_suggested_cutoff(pseudos=pseudo_dict.values())
        ctx.calc_params['SYSTEM']['ecutwfc']=ecutwfc
        ctx.calc_params['SYSTEM']['ecutrho']=ecutrho
        ctx.calc_params['CONTROL']['max_seconds']=ctx.max_seconds
        ctx.calc_params['CONTROL']['calculation']='scf'

        inputs.parameters = ParameterData(dict=copy.deepcopy(ctx.calc_params))
        inputs.pseudo = pseudo_dict

        # input structure
        inputs.structure = ctx.supercell
        # Calculation params
        inputs._options.max_wallclock_seconds = ctx.max_walltime_seconds
        inputs._options.resources = {"num_machines": ctx.num_machines}
        newcalculation = asyncd(proc, **inputs)
        ctx.calculation_seq.append(newcalculation)
        return ResultToContext(lastcalculation=newcalculation)


    def _step_adjust_parameters_2_NVT(self, ctx):
        print "adjusting parameters to NVT"
        ctx.calc_params['CONTROL']['calculation'] = 'md'
        ctx.calc_params['CONTROL']['restart_mode'] = 'restart'
        ctx.calc_params['CONTROL']['dt'] = ctx.timestep_au_ry
        ctx.calc_params['IONS'] = {}
        ctx.calc_params['IONS']['tempw'] = ctx.target_temperature_K
        ctx.calc_params['IONS']['tolp'] = ctx.temperature_tolerance
        ctx.calc_params['IONS']['ion_temperature'] = ION_TEMP_CONTROL
        

        
    def _is_not_thermalized(self, ctx):
        print "check thermalized"   
        return ctx.time_to_thermalize_fs > 0


    def _step_NVT(self, ctx):
        """
        Run a step of a microcanonical simulation, where the number of steps
        is given by the remaining thermalization time.
        """
        # I need to get the last calculation that ran in this step
        # Since I am putting the result in the context,
        # the only way I see is to get it via the ctx.lastcalculation
        # Problem: that is a dictionary that stores all the outputs,
        # To get to the calculation I do the following really nasty trick

        restart_from_scf = len(ctx.calculation_seq_nvt) == 0


        print self._calc.pk, self._calc.uuid, "at _step_NVT({})".format(len(ctx.calculation_seq_nvt))

        remote = ctx.lastcalculation['remote_folder']
        oldcalc, = remote.get_inputs()



        if restart_from_scf:
            assert isinstance(oldcalc, CalculationFactory(str(self.inputs.plugin_name_scf)))
        else:
            assert isinstance(oldcalc, CalculationFactory(str(self.inputs.plugin_name_md)))

        # I need to create a restart from the old calculation
        # newcalc=oldcalc.create_restart()
        # This throws: AssertionError: Cannot add incoming links to a sealed calculation node
        # Very inconvenient
        proc = CalculationFactory(str(self.inputs.plugin_name_md)).process()
        inputs = proc.get_inputs_template()
        pseudo_dict = {}
        for k, v in oldcalc.get_inputs_dict().items():
            if isinstance(v, UpfData):
                pseudo_dict[k.replace('pseudo_', '')]=v
            elif k == 'parent_calc_folder':
                continue
            elif isinstance(v, (Code,  Data)):
                setattr(inputs, k, v)
        inputs.pseudo = pseudo_dict
        inputs.code = Code.get_from_string(ctx.code_name_md)
            
        ctx.calc_params['CONTROL']['max_seconds'] = ctx.max_seconds
        ctx.calc_params['CONTROL']['nstep'] = int(ctx.time_to_thermalize_fs / ctx.timestep_in_fs)+1

        inputs.parameters = ParameterData(dict=copy.deepcopy(ctx.calc_params))
        inputs.parent_folder = remote
        inputs._options.resources = {"num_machines": ctx.num_machines}
        inputs._options.max_wallclock_seconds = ctx.max_walltime_seconds
        newcalculation = asyncd(proc, **inputs)
        ctx.calculation_seq_nvt.append(newcalculation)
        ctx.calculation_seq.append(newcalculation)
        return ResultToContext(lastcalculation=newcalculation)

    def _step_analyze_NVT(self, ctx):
        print "analyzing_NVT"
        
        self._calc.set_extra('iterations', ctx.iterations)
        self._calc.set_extra('calculation_seq', ctx.calculation_seq)

        try:
            sim_time_fs = ctx.lastcalculation['output_trajectory'].get_attr('sim_time_fs')
        except AttributeError:
            sim_time_fs = ctx.timestep_in_fs*ctx.lastcalculation['output_trajectory'].get_attr('array|steps.0')
        ctx.time_to_thermalize_fs = ctx.time_to_thermalize_fs - sim_time_fs


    def _step_adjust_parameters_NVT_2_NVE(self, ctx):
        """
        I am done with NVT, switching to NVE, for this I am changing the
        parameters accordingly
        """
        print "Adjusting parameters to NVE"
        del ctx.calc_params['IONS']['tempw']
        del ctx.calc_params['IONS']['tolp']
        ctx.calc_params['IONS']['ion_temperature'] = 'not_controlled'
        if ctx.max_nstep is None:
            del ctx.calc_params['CONTROL']['nstep']
        else:
            ctx.calc_params['CONTROL']['nstep'] = ctx.max_nstep


    def _is_not_finished(self, ctx):
        return (
                ctx.conductivity_rel_err < 0 or 
                ctx.conductivity_rel_err < ctx.conductivity_rel_err_threshold 
            )


    def _step_NVE(self, ctx):
        ctx.iterations = ctx.iterations + 1
        print self._calc.pk, self._calc.uuid, "at _step_NVE({})".format(ctx.iterations)
        remote = ctx.lastcalculation['remote_folder']
        oldcalc, = remote.get_inputs()
        assert isinstance(oldcalc, CalculationFactory(str(self.inputs.plugin_name_md)))

        ctx.calculation_seq.append(oldcalc.pk)

        self._calc.set_extra('iterations', ctx.iterations)
        self._calc.set_extra('calculation_seq', ctx.calculation_seq)

        newprocess = CalculationFactory(str(self.inputs.plugin_name_md)).process()
        inputs = newprocess.get_inputs_template()
        pseudo_dict = {}
        for k, v in oldcalc.get_inputs_dict().items():
            if isinstance(v, UpfData):
                pseudo_dict[k.replace('pseudo_', '')]=v
            elif k == 'parent_calc_folder':
                continue
            elif isinstance(v, (Code,  Data)):
                setattr(inputs, k, v)
        inputs.pseudo = pseudo_dict
        ctx.calc_params['CONTROL']['max_seconds'] = ctx.max_walltime_seconds - 10
        inputs.parameters = ParameterData(dict=copy.deepcopy(ctx.calc_params))
        inputs.parent_folder = remote
        inputs._options.resources = {"num_machines": ctx.num_machines}
        inputs._options.max_wallclock_seconds = ctx.max_walltime_seconds
        newcalculation = asyncd(newprocess, **inputs)
        ctx.calculation_seq.append(newcalculation)
        ctx.calculation_seq_nve.append(newcalculation)
        return ResultToContext(lastcalculation=newcalculation)

    def _step_analyze_NVE(self, ctx):
        from mdtools.libs.mdlib.trajectory_analysis import TrajectoryAnalyzer
        
        trajectories = np.concatenate(
            [
                load_node(calc_id).out.output_trajectory.get_array('positions')
                for calc_id
                in 
                ctx.calculation_seq_nve[:-1]
                
            ]
        )
        ta = TrajectoryAnalyzer()
        ta.set_structure(structure=ctx.supercell)
        ta.set_trajectory(trajectory=trajectories, timestep_in_fs=ctx.timestep_in_fs)
        
        ta.get_msd_fort(
                vaf_is_0_fs=ctx.vaf_is_0_fs,
                block_length_fs=ctx.block_length_fs,
                max_t_msd_fs=ctx.max_t_msd_fs,
                species_of_interest=ctx.species_of_interest,
            )
        ctx.conductivity_results = ta.calculate_conductivity(ctx.target_temperature_K)
        ctx.conductivity_rel_err = ctx.conductivity_results[
                ctx.species_of_interest
            ]['conductivity_std_SI'] / ctx.conductivity_results[
                ctx.species_of_interest
            ]['conductivity_mean_SI']

        
    def _step_results(self, ctx):
        print "FINISHED:"
        output_params = {}
        output_params['calculation_seq'] = ctx.calculation_seq
        self.out('output_params', ParameterData(dict=output_params))
        print ctx['calculation_seq']
        print ctx['calculation_seq_nvt']
        print self._calc.get_extra('calculation_seq')

    def get_pseudos(self):
        """
        Pseudo family is stored in the input parameters
        """
        pseudo_dict = {}
        
        for kind in self.inputs.structure.kinds:
            element = kind.symbols[0]
            qb = QueryBuilder()
            qb.append(Group, filters={'name':self.inputs.pseudo_family_name.value})
            qb.append(UpfData, member_of=Group, filters={'attributes.element':element})
            try:
                pseudo, = qb.first()
            except TypeError:
                raise Exception ("Pseudo for  {} not found".format(element))
            pseudo_dict[kind.name] = pseudo
        return pseudo_dict

    def get_suggested_cutoff(self, pseudos=None):
        if pseudos is None:
            pseudos = self.get_pseudos().values()

        try:
            cutoffs_n_duality = [
                    (p.get_extra('cutoff'), p.get_extra('duality'))
                    for p
                    in pseudos
                ]
        except AttributeError as e:
            raise AttributeError(
                    "raised when asking for cutoff and duality\n"
                    "for pseudos {}\n"
                    "{}".format(pseudos, e)
                )
        ecutwfc = 0.
        ecutrho = 0.
        for cutoff, duality in cutoffs_n_duality:
            ecutwfc = max([ecutwfc, cutoff])
            ecutrho = max([ecutrho, cutoff*duality])
        return ecutwfc, ecutrho

    def get_nelec(self, input_structure):
        pseudos  = self.get_pseudos()
        kinds_used = [s.kind_name for s in input_structure.sites]
        nelec = 0.
        for kname in set(kinds_used):
            pseudo_kname = pseudos[kname]
            nat_ityp = kinds_used.count(kname)
            nelec += pseudo_kname.get_extra('valence')*nat_ityp
        return nelec
