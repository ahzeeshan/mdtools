#!/usr/bin/env python
import numpy as np, sys, os, re





def read_dump(dumpfile, overwrite=False, verbosity=False):
    """
    Parse a LAMMPS dumpfile
    """

    DUMP_REGEX  = re.compile("""
    ITEM:\s*TIMESTEP \s* (?P<timestep>\d+) \s* # Reading the timestep
    ITEM:\s*NUMBER\s+OF\s+ATOMS \s* (?P<nat>\d+) \s* # Reading the NUMBER OF ATOMS
    ITEM:\s+BOX\s+BOUNDS\s+pp\s+pp\s+pp\s*(\d+(\.\d+)?\s+\d+(\.\d+)?\s*){3}# Box bounds, I don't care right now
    ITEM:\s+ATOMS(?P<atomsf>(\s+[a-z]+)+)\s* # Atoms format, reading it out
    (?P<atomblock>
    (([\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)?[ \t]*)+[\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)?\s*)+)
    """, re.X | re.M)

    def read_single_timestep(timestep_match):
        if verbosity:
            print timestep_match.group('timestep')
        lines = [line.split() for line in timestep_match.group('atomblock').split('\n')[:-1]]
        if len(lines) != int(timestep_match.group('nat')):
            for line in lines:
                print '  '.join(line)
            raise Exception(
                    " Length of lines found {}\n"
                    "does not coincide with number of atoms {}\n"
                    "this happened on timestep {}\n"
                    "".format(len(lines), int(timestep_match.group('nat')), int(timestep_match.group('timestep')))
                )
        # The following sorts the lines by the atomid, which is in position id_index of the line.
        # I pop this quantity, so it's not stored in the end!
        sorted_lines = sorted([
                (int(line[id_index]), map(float, line))
                for line in lines
            ])
        quantities = zip(*sorted_lines)[1]

        return np.array(quantities, dtype=np.float64)

    with open(dumpfile) as f:
        txt=f.read()

    match1 = DUMP_REGEX.search(txt)

    if match1 is None:
        raise Exception("Could not read dumpfile")

    nat = int(match1.group('nat'))
    atomsf = match1.group('atomsf').split()

    for f in ('id', 'xu', 'yu', 'zu', 'vx', 'vy', 'vz', 'fz', 'fy', 'fz'):
        assert f in atomsf, "{} missing from format".format(f)

    id_index = atomsf.index('id')
    if verbosity:
        print "         starting at timestep ", int(match1.group('timestep'))
        print "         number of atoms is   ", nat
        print "         atoms format is      ", ','.join(atomsf)
        print "         Index of atomid is   ", id_index

    trajectory = np.array([read_single_timestep(match) for match in DUMP_REGEX.finditer(txt)], dtype=np.float64)

    if verbosity:
        print "         I have a trajectory of shape", trajectory.shape

    position_indices = [atomsf.index(s) for s in ('xu', 'yu', 'zu')]
    velocities_indices = [atomsf.index(s) for s in ('vx', 'vy', 'vz')]
    forces_indices = [atomsf.index(s) for s in ('fz', 'fy', 'fz')]
    positions = trajectory[:,:,position_indices]
    velocities = trajectory[:,:,velocities_indices]
    forces = trajectory[:,:,forces_indices]

    return dict(positions=positions, velocities=velocities, forces=forces)


def read_thermo(thermofile, overwrite=False):
    """
    Read the thermo expression from the lammps log.
    """
    # Load the text into memory
    with open(thermofile) as f:
        txt = f.read()
    # Now I am reading aspecific line in the beginning of the log file or output file
    # that looks for example as follows:
    # thermo_style custom step v_Jx v_Jy v_Jz v_Jcx v_Jcy v_Jcz press temp pe ke etotal

    style_match = re.search("thermo_style custom (?P<style>.*)", txt)
    if style_match is None:
        raise Exception("I did not match the thermo style")
    # I need the length N, of this:
    thermo_length =  len(style_match.group('style').split())
    # Now I compile a regular expression that searches for a line that contains only
    # N floats. Hopefully this only contains the thermo information
    thermo_regex = re.compile('^(?P<thermo>([ \t]+[\-|\+]?(\d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)?){{{}}})\s*$'.format(thermo_length), flags=re.X|re.M)
    thermo_arr = np.array([
            map(float, match.group('thermo').split())
            for match in thermo_regex.finditer(txt)
        ])
    print "Read thermo has read an array of shape", thermo_arr.shape
    return {style_spec:thermo_arr[idx] for idx, style_spec in enumerate(style_match.group('style').split())}


def read_lammps_data(inputf, replace_kinds=None):
    """
    I read a data file that was used in lammps using ASE.io.read utility.
    :param str inputf: A valid filename
    :param dict replace_kinds:
        A dictionary to replace the atomic numbers.
        If read directly from the lammps data, they might be messed up.
    """
    from ase.io import read
    atoms = read(inputf, format='lammps-data')
    if replace_kinds is not None:
        if not isinstance(replace_kinds, dict):
            raise Exception("replace_kinds is not a dictionary")
        new_atomic_numbers = [replace_kinds.get(an, an) for an in atoms.get_atomic_numbers()]
        atoms.set_atomic_numbers(new_atomic_numbers)
    return atoms


def parse_lammps(
        inputfile=None, thermofile=None, dumpfile=None, replace_kinds=None,
        savefiles=False, overwrite=False, verbosity=False):

    res = dict()
    if inputfile is not None:

        print "Reading atoms"
        atoms = read_lammps_data(inputfile, replace_kinds=replace_kinds)
        print "   Done, I received", atoms.get_chemical_formula()
        res['atoms'] = atoms

        if savefiles:
            from pickle import dump
            with open('atoms.pickle', 'w') as f:
                dump(atoms, f)
    if thermofile is not None:
        print "Reading thermo-information from", thermofile
        for k,arr in read_thermo(thermofile).items():
            print "     {:<20} {}".format(k, arr.shape)
            res[k] = arr
            if savefiles:
                np.save('{}.npy'.format(k), arr)
    if dumpfile is not None:
        print "Reading dump in", dumpfile
        print "   Done, I received:"
        for k,arr in read_dump(dumpfile).items():
            print "     {:<20} {}".format(k, arr.shape)
            res[k] = arr
            if savefiles:
                np.save('{}.npy'.format(k), arr)
    return res

if __name__ == '__main__':
    from argparse import ArgumentParser
    from json import loads
    ap = ArgumentParser("python parse-lammps.py")
    ap.add_argument('-i', '--inputfile')
    ap.add_argument('--save', action='store_true', help='save the files')
    ap.add_argument('-d', '--dumpfile')
    ap.add_argument('-t', '--thermofile')
    ap.add_argument('-o', '--overwrite', action='store_true')
    ap.add_argument('-v', '--verbosity', action='store_true')
    ap.add_argument('-k', '--replace-kinds', help="""
Replace the kinds of the structure read from the lammps data.
Has to be a valid string for a json decoder.
I.e. if you want to change atomtype 2 in lammps with 8 (oxygen), pass -k '{2:8}'""")
    pa = ap.parse_args(sys.argv[1:])
    
    replace_kinds = {int(i):j for i,j in loads(pa.replace_kinds).iteritems()} if pa.replace_kinds is not None else None

    res = parse_lammps(
            inputfile=pa.inputfile, dumpfile=pa.dumpfile, thermofile=pa.thermofile,
            savefiles=pa.save,
            replace_kinds=replace_kinds, overwrite=pa.overwrite, verbosity=pa.verbosity)
    for k,v in res.items():
        print k, type(v)
