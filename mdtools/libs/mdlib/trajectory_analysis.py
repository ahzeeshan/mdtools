#!/usr/bin/env python
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"
"""
This is the module to calculate mean square displament (MSD),
velocity autocorrelation function (VAF)
and diffusion coefficient (D).

This is mainly taken care of by the class
:func:`TrajectoryAnalyzer` below.
"""

import sys
import numpy as np
import itertools

from scipy.stats import linregress
from scipy.stats import sem as standard_error_of_mean

from ase.data.colors import jmol_colors
from ase.data import atomic_masses, atomic_numbers
from ase.data.colors import jmol_colors, cpk_colors

import matplotlib
matplotlib.use('pdf')
from matplotlib import pyplot as plt
from matplotlib.gridspec  import GridSpec
from matplotlib.patches import Rectangle

from colorsys import rgb_to_hls, hls_to_rgb



from mdtools.libs.struclib.structure_wizard import structure_wizard


jmol_colors[1] = [0.,0.,1.] # Overwriting the white of H, impossible to see!
e2 =  2.5669697614569647e-38
kB =  1.38064852e-23
kB_ev = 8.6173303e-5
kB_au = 3.166810800209422e-06
bohr_to_ang = 0.52917721092
amu_kg = 1.660539040e-27
amu_au =  1822.888486  # A stamdard atomic mass in atomic units (where mass electron =1 )
timeau_to_sec = 2.418884326155573e-17
timeau_to_fs = 2.418884326155573e-2
hartree_to_ev = 27.2113834506
class TrajectoryAnalyzer():
    """
    Class analyzing trajectories and sampling statistics
    """
    def __init__(self,  **kwargs):
        """
        Initializes an instance of the class TrajectoryAnalyzer.
        Arguments passed can include:
        *   ``log``: A file to write to, else will print to standard output
        *   ``verbosity``: A boolean to determine printing level
        *   ``trajectory``: The trajectory to analyze, calls :func:`set_trajectory`
        *   ``structure``: The structure that the trajectory was started from, mainly to get the atomic species, call to :func:`set_structure`
        """
        self.log = kwargs.pop('log', sys.stdout)
        self._verbosity = kwargs.pop('verbosity', 0)

        if 'trajectory' in kwargs.keys():
            self.set_trajectory(kwargs.pop('trajectory'))
        if 'structure' in kwargs.keys():
            self.set_structure(kwargs.pop('structure'))
        if kwargs:
            raise Exception("Unknown keywords: {}".format(kwargs.keys()))


    def set_trajectory(self, trajectory, timestep_in_fs, **kwargs):
        self.set_trajectories([trajectory], timestep_in_fs, **kwargs)


    def set_trajectories(
            self, positions, timestep_in_fs, recenter=False, tags=None,
            velocities=None, pos_units='angstrom', vel_units='a_p_fs', discard_fs=None, shorten_if_different=False):
        """
        Set the trajectory to be analyzed.
        The trajectory needs to be in the same form as return by an instance
        of TrajectoryData in the database.
        Therefore, it expects the positions to be in Angstrom!

        :param positions: A list of np.arrays
        :param timestep_in_fs: The timestep in fs as a float.

        """
        from difflib import recenter_positions, recenter_velocities


        try:
            self._masses=self.structure.get_masses()
        except AttributeError:
            self._masses=self.structure.get_ase().get_masses()

        self._positions = []
        self._velocities = []
        self._has_velocities = True
        self._tags = tags # tags for the trajectories
        self.timestep_in_fs = timestep_in_fs

        if discard_fs is not None:
            self._discard_fs = float(discard_fs)
            self._discarding = True
            self._discard_dt = int(self._discard_fs / self.timestep_in_fs)
        else:
            self._discard_dt = 0
            self._discard_fs = 0.0
            self._discarding = False


        for i, pos_array in enumerate(positions):
            if self._discarding:
                pos_array = pos_array[self._discard_dt:]
            if not isinstance(pos_array, np.ndarray):
                raise Exception(
                    'I was given a {} instead of a numpy array'
                    ''.format(type(pos_array))
                )

            if pos_units == 'angstrom':
                pos_array = pos_array

            elif pos_units in ('bohr', 'atomic'):
                pos_array = bohr_to_ang*pos_array
            else:
                raise NotImplementedError
            if self._verbosity > 0:
                print "       pos_array of shape {}".format(pos_array.shape)

            if recenter:
                # Now, the user wants to recenter, for which I have to provide the
                # masses since we calculate the COM.
                # But there might be cases when the pos_array has a different size
                #
                nat_pos, nat_mass = pos_array.shape[1], self._masses.shape[0]
                if nat_pos != nat_mass:
                    # Important: I assume that the positions given are not included after a certain atomindex
                    # as for the pinball dynamics, for example
                    masses = self._masses[:nat_pos]
                    factors = self._factors[:nat_pos]
                else:
                    masses = self._masses
                    factors = self._factors

                pos_array = recenter_positions(pos_array, masses, factors)
                self._positions.append(pos_array)
            else:
                self._positions.append(pos_array)
            if self._has_velocities:
                try:
                    vel_array = velocities[i]

                    if self._discarding:
                        vel_array = vel_array[self._discard_dt:]

                    if not isinstance(vel_array, np.ndarray):
                        raise Exception(
                            'I was given a {} instead of a numpy array'
                            ''.format(type(pos_array))
                        )

                    # Convert everything to angstrom per femtosecond a_p_fs
                    if vel_units == 'a_p_fs':
                        vel_array = vel_array

                    elif vel_units in ('cp', ):
                        # bohr / atomic time unit (1.03275e-15 s
                        vel_array = bohr_to_ang / timeau_to_fs * vel_array
                    elif vel_units in ('pw', 'atomic'):
                        # bohr / atomic time unit (1.03275e-15 s
                        vel_array = bohr_to_ang / timeau_to_fs / 2. * vel_array
                    elif vel_units == 'lammps_electron':
                        #~ vel_array = bohr_to_ang / 1.03275 * vel_array
                        #~ vel_array = bohr_to_ang * 1.03275 * vel_array
                        vel_array = bohr_to_ang * vel_array
                    else:
                        raise NotImplementedError("I have not implemented {}".format(vel_units))
                    if self._verbosity > 0:
                        print "       vel_array of shape {}".format(vel_array.shape)
                    assert pos_array.shape == vel_array.shape, "incommensurate arrays"
                    if recenter:
                        vel_array = recenter_velocities(vel_array, self._masses, self._factors)
                        self._velocities.append(vel_array)
                    else:
                        self._velocities.append(vel_array)
                except Exception as e:
                    if self._verbosity:
                        print e
                    self._has_velocities = False


        nstep_set, nat_set = set(), set()
        for t in self._positions:
            try:
                nstep_, nat_, _ = t.shape
            except Exception as e:
                print t.shape
                raise e
            nstep_set.add(nstep_)
            nat_set.add(nat_)
        # For now I'm requiring all trajectories to be of the same size, maybe
        # at least for nstep that can be dropped?

        if len(nstep_set) == 1:
            self._nstep = nstep_set.pop()
        else:
            print "WARNING, trajectories do not have the same length"
            if shorten_if_different:
                self._nstep = min(nstep_set)
                for idx, arr in enumerate(self._positions):
                    self._positions[idx] = arr[:self._nstep]
                if self._has_velocities:
                    for idx, arr in enumerate(self._velocities):
                        self._velocities[idx] = arr[:self._nstep]

        self._nat = nat_set.pop()
        if nat_set:
            raise Exception("incommensurate arrays")

    def read_trajectories(self, pos_files, timestep_in_fs, vel_files=None, format='xyz',**kwargs):
        import re
        if format == 'xyz':
            pass

        if not isinstance(pos_files, (list,tuple)):
            pos_files = [pos_files]


        positions_l = list()
        if vel_files is not None:
            velocities_l = list()
        else:
            velocities_l = None
        for idx, fname in enumerate(pos_files):
            if format=='xyz':

                pos_regex = re.compile("""
                    ^                                                                             # Linestart
                    [ \t]*                                                                        # Optional white space
                    (?P<x> [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)? ) [ \t]+  # Get x
                    (?P<y> [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)? ) [ \t]+  # Get y
                    (?P<z> [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)?)          # Get z
                    """, re.X | re.M)

                pos_block_regex = re.compile("""
                    ^(\s*[+-]?(\d*[\.]\d+ |\d+[\.]?\d*) ([E|e][+-]?\d+)? ([ \t]+[+-]?(\d*[\.]\d+ |\d+[\.]?\d*) ([E|e][+-]?\d+)?){2}\s*)+
                    """, re.X | re.M)
            elif format == 'axyz':
                pos_regex = re.compile("""
                        ^                                                                             # Linestart
                        [ \t]*                                                                        # Optional white space
                        (?P<sym>[A-Za-z]+[A-Za-z0-9]*)\s+                                             # get the symbol
                        (?P<x> [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)? ) [ \t]+  # Get x
                        (?P<y> [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)? ) [ \t]+  # Get y
                        (?P<z> [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)? )         # Get z
                        """, re.X | re.M)

                pos_block_regex = re.compile("""
                    ([A-Za-z]+[A-Za-z0-9]*\s+([ \t]+ [\-|\+]?  ( \d*[\.]\d+  | \d+[\.]?\d* )  ([E | e][+|-]?\d+)?){3}\s*)+
                    """, re.X | re.M)

            else:
                raise Exception("Can't deal with {}".format(format))
            with open(fname) as f:


                positions = np.array(
                    [
                        [
                            (
                                float(pos_match.group('x')),
                                float(pos_match.group('y')),
                                float(pos_match.group('z'))
                            )
                            for pos_match in pos_regex.finditer(match.group(0))
                        ]
                        for match in pos_block_regex.finditer(f.read())
                    ]
                )
            positions_l.append(positions)

            if vel_files is not None:
                with open(vel_files[idx]) as f:

                    velocities = np.array(
                        [
                            [
                                (
                                    float(pos_match.group('x')),
                                    float(pos_match.group('y')),
                                    float(pos_match.group('z'))
                                )
                                for pos_match in pos_regex.finditer(match.group(0))
                            ]
                            for match in pos_block_regex.finditer(f.read())
                        ]
                    )
                velocities_l.append(velocities)


        self.set_trajectories(
            positions_l,
            velocities=velocities_l,
            timestep_in_fs=timestep_in_fs,
            **kwargs)


    def set_temperature(self, temperature):
        self.temperature = temperature

    def _get_indices_of_interest(self, atomic_species, start):
        return np.array(
                    [
                        index
                        for index, atom
                        in enumerate(self._chemical_symbols, start=start) # Python to fortran index i -> i+1
                        if atom == atomic_species
                    ],
                    dtype='i'
                )

    def _get_factors(self, atomic_species):
        return np.array(
                    [
                        1 if atom == atomic_species else 0
                        for atom
                        in self._chemical_symbols
                    ],
                    dtype='i'
                )

    def set_structure(self, structure, species_of_interest=None, factors=None):
        """
        Set the structure with the help of :func:`~carlo.codes.voronoi.structure_imports.structure_wizard`
        """

        try:
            from aiida.orm.data.structure import StructureData
            if isinstance(structure, StructureData):
                self.structure = structure.get_ase()
                self._chemical_symbols = structure.get_site_kindnames()
                #~ print "HERE"
            else:
                raise Exception("Not an instance")
        except Exception as e:
            s = structure_wizard(structure, need_dict=True)
            self.structure = s['ase']
            self._chemical_symbols = s['chemical_symbols']

        if species_of_interest is None:
            self.species_of_interest =  list(set(self._chemical_symbols))
        elif isinstance(species_of_interest, (tuple, list)):
            self.species_of_interest =  species_of_interest
        else:
            self.species_of_interest = [species_of_interest]
        for atomic_species in self.species_of_interest:

            indices_of_interest = self._get_indices_of_interest(atomic_species, 0)
            nat_of_interest = len(indices_of_interest)

            if nat_of_interest == 0:
                raise Exception(
                    "empty array when looking for\n"
                    "incides for {}"
                    "".format(atomic_species)
                )
        if factors is not None:
            self._factors = factors
        else:
            self._factors = [1]*len(self._chemical_symbols)
        if not isinstance(self._factors, (list, tuple)):
            raise Exception("Factors has to be a list or tuple")
        if not all([isinstance(_, int) for _ in  self._factors]):
            raise Exception("Factors has to be a list or tuple of integers")



    def get_vaf(self, integration='trapezoid', **kwargs):

        from difflib import calculate_vaf_specific_atoms, get_com_velocities

        try:
            structure = self.structure
            self._chemical_symbols
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        try:
            velocities = self._velocities
            timestep_in_fs = self.timestep_in_fs
            nstep_set, nat_set = set(), set()
            if not self._has_velocities:
                raise AttributeError
            for v in self._velocities:
                nstep_, nat_, _ = v.shape
                nstep_set.add(nstep_)
                nat_set.add(nat_)

            nstep = nstep_set.pop()
            nat = nat_set.pop()

            if nstep_set or nat_set:
                raise Exception("incommensurate arrays")

        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_trajectory method to give me velocities"
                "\n\n\n"
            )
        log = self.log # set in __init__
        species_of_interest = kwargs.pop("species_of_interest", self.species_of_interest)

        stepsize_t = kwargs.pop('stepsize_t', 1)
        #~ if stepsize_t > 1:
            #~ raise DeprecationWarning("Don't pyt the stepsize > 1 when computing the VAF!")

        stepsize_tau = kwargs.pop('stepsize_tau', 1)
        t_start_fit_fs = kwargs.pop('t_start_fit_fs', None)
        t_start_fit_dt = kwargs.pop('t_start_fit_dt', None)

        if t_start_fit_fs and t_start_fit_dt:
            raise Exception("You cannot set both 't_start_fit_fs' and 't_start_fit_dt'")
        elif isinstance(t_start_fit_fs, float):
            t_start_fit_dt  = int(t_start_fit_fs / timestep_in_fs)
        elif isinstance(t_start_fit_dt, int):
            block_length_fs  = t_start_fit_dt * timestep_in_fs
        else:
            raise Exception(
                "Please set the time to start the fit with keyword t_start_fit_fs or t_start_fit_dt"
            )

        t_end_fit_fs = kwargs.pop('t_end_fit_fs', None)
        t_end_fit_dt = kwargs.pop('t_end_fit_dt', None)

        if t_end_fit_fs and t_end_fit_dt:
            raise Exception("You cannot set both 't_end_fit_fs' and 't_end_fit_dt'")
        elif isinstance(t_end_fit_fs, float):
            t_end_fit_dt  = int(t_end_fit_fs / timestep_in_fs)
        elif isinstance(t_end_fit_dt, int):
            block_length_fs  = t_end_fit_dt * timestep_in_fs
        else:
            raise Exception(
                "Please set the time to end the fit with keyword t_end_fit_fs or t_end_fit_dt"
            )

        t_end_vaf_fs = kwargs.pop('t_end_vaf_fs', None)
        t_end_vaf_dt = kwargs.pop('t_end_vaf_dt', None)

        if t_end_vaf_dt and t_end_vaf_fs:
            raise Exception("You specified both 't_end_vaf_fs' and 't_end_vaf_dt'")
        elif isinstance(t_end_vaf_fs, float):
            t_end_vaf_dt  = int(t_end_vaf_fs / timestep_in_fs)
        elif isinstance(t_end_vaf_dt, int):
            t_end_vaf_fs  = t_end_vaf_dt * timestep_in_fs
        else:
            t_end_vaf_fs = t_end_fit_fs
            t_end_vaf_dt = t_end_fit_dt

        nr_of_t = t_end_vaf_dt / stepsize_t

        # Checking if I have to partition the trajectory into blocks (By default just 1 block)
        block_length_fs = kwargs.pop('block_length_fs', None)
        block_length_dt = kwargs.pop('block_length_dt', None)

        nr_of_blocks = kwargs.pop('nr_of_blocks', None)

        do_com =  kwargs.pop('do_com', False)
        if kwargs:
            raise Exception("Uncrecognized keywords: {}".format(kwargs.keys()))

        if block_length_fs and block_length_dt :
           raise Exception("You cannot set both 'block_length_fs' and 'block_length_dt'")
        elif ( block_length_dt or block_length_fs ) and nr_of_blocks:
            raise Exception("You cannot set both 'a block length' and 'nr_of_blocks'") #
            # Maybe allow this in the future, at the cost of not the whole trajectory being used:
        elif isinstance(block_length_fs, float):
            block_length_dt  = int(block_length_fs / timestep_in_fs)
        elif isinstance(block_length_dt, int):
            block_length_fs  = block_length_dt * timestep_in_fs
        elif isinstance(nr_of_blocks, int):
            nr_of_blocks = nr_of_blocks
        else:
            nr_of_blocks=1
            #~ raise Exception(
                #~ "Please set the block length in femtoseconds (kw block_length_fs or block_length_dt) or define the\n"
                #~ "number of blocks (kw nr_of_blocks)"
            #~ )
        self.vaf_all_species =  []
        self.vaf_results_dict = dict(
            t_end_vaf_fs    =   t_end_vaf_fs,
            t_end_vaf_dt    =   t_end_vaf_dt,
            stepsize_t      =   stepsize_t,
            stepsize_tau    =   stepsize_tau,
            timestep_in_fs  =   timestep_in_fs,
            nr_of_t         =   nr_of_t,
            t_start_fit_dt  =   t_start_fit_dt,
            t_start_fit_fs  =   t_start_fit_fs,
            t_end_fit_dt    =   t_end_fit_dt,
            t_end_fit_fs    =   t_end_fit_fs,
            species_of_interest=species_of_interest,
        )

        self.D_from_vaf_averaged = []
        for atomic_species in species_of_interest:
            indices_of_interest = self._get_indices_of_interest(atomic_species, start=1)
            if do_com:
                indices_of_interest = [1]
                prefactor = len(self._get_indices_of_interest(atomic_species, start=0))
            else:
                indices_of_interest = self._get_indices_of_interest(atomic_species, start=1)
                prefactor = 1


            nat_of_interest     = len(indices_of_interest)
            vaf_this_species = []
            slopes_n_intercepts = []
            means_of_integral = []
            for vel_array in velocities:
                total_steps = len(vel_array)
                total_time = total_steps*self.timestep_in_fs
                if nr_of_blocks:
                    block_length_dt = (total_steps - t_end_vaf_dt)  / nr_of_blocks
                    block_length_fs = block_length_dt * timestep_in_fs
                else:
                    nr_of_blocks   = ( total_steps - t_end_vaf_dt ) / block_length_dt

                if do_com:
                    factors = self._get_factors(atomic_species)
                    vel_array = get_com_velocities(vel_array, self._masses, factors, nstep, nat)
                    nstep, nat, _= vel_array.shape

                if self._verbosity > 0:
                    log.write(
                            '\n    ! Calculating VAF for atomic species {}\n'
                            '      Structure contains {} atoms of type {}\n'
                            '      Max time (fs)     = {}\n'
                            '      Max time (dt)     = {}\n'
                            '      Stepsize for t    = {}\n'
                            '      stepsize for tau  = {}\n'
                            '      nr of timesteps   = {}\n'
                            '      nr of blocks      = {}\n'
                            '      Block length (fs) = {}\n'
                            '      Block length (dt) = {}\n'
                            '      Calculating VAF with fortran subroutine fortvaf.calculate_vaf_specific_atoms\n'
                            ''.format(
                                atomic_species, nat_of_interest, atomic_species,
                                t_end_vaf_fs, t_end_vaf_dt, stepsize_t, stepsize_tau, nr_of_t,
                                nr_of_blocks, block_length_fs, block_length_dt
                            )
                        )

                res = calculate_vaf_specific_atoms(
                    vel_array,
                    indices_of_interest,
                    stepsize_t,
                    stepsize_tau,
                    nr_of_t,
                    nr_of_blocks,
                    block_length_dt,
                    timestep_in_fs*stepsize_t, # Integrating with timestep*0.1 / 3.
                    integration,
                    nstep,
                    nat,
                    nat_of_interest
                )

                range_for_t = timestep_in_fs*stepsize_t*np.arange(t_start_fit_dt/stepsize_t, t_end_fit_dt/stepsize_t)

                #~ for iblock, block in enumerate(msd_isotrop_this_species_this_traj, start=1):


                for block_vaf, integrated_vaf in zip(*res):
                    D =  0.1 / 3. *prefactor* integrated_vaf  # transforming A^2/fs -> cm^2 /s, dividing by three to get D
                    vaf_this_species.append((block_vaf, D))
                    slope, intercept, _, _, _ = linregress(
                            range_for_t,
                            D[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t]
                        )
                    slopes_n_intercepts.append((slope, intercept))
                    means_of_integral.append(D[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t].mean())

            D_vaf = zip(*vaf_this_species)[1]
            D_mean = np.mean(D_vaf, axis=0)
            D_std  = np.std(D_vaf, axis=0)
            D_sem  = D_std / np.sqrt(len(D_vaf) - 1)

            D_upper_sem = D_mean + D_sem
            D_lower_sem = D_mean - D_sem
            D_upper_std = D_mean + D_std
            D_lower_std = D_mean - D_std

            upper_bound_sem = D_upper_sem[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t].mean()
            lower_bound_sem = D_lower_sem[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t].mean()
            upper_bound_std = D_upper_std[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t].mean()
            lower_bound_std = D_lower_std[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t].mean()
            self.D_from_vaf_averaged.append((
                    D_mean, D_sem, D_std,
                    0.5*(upper_bound_sem + lower_bound_sem),
                    0.5*(upper_bound_sem - lower_bound_sem),
                    0.5*(upper_bound_std + lower_bound_std),
                    0.5*(upper_bound_std - lower_bound_std)
                ))
            self.vaf_results_dict[atomic_species] = dict(
                    slopes_n_intercepts=slopes_n_intercepts,
                    means_of_integral=means_of_integral,
                    means_of_means=np.mean(means_of_integral),
                    std_of_means=np.std(means_of_integral),
                    sem_of_means=standard_error_of_mean(means_of_integral)
                )
            self.vaf_all_species.append(vaf_this_species)


        return self.vaf_results_dict, np.array(self.vaf_all_species)


    def velocities_fourier(self, decorrelation_time_fs):
        """
        TODO: block analysis using the signal

        """
        def myfilter(arr, window, freq):
            # I do two things:
            # First I average the function to remove the crazy wiggles
            newarr = np.empty(arr.shape)
            for istep in range(arr.size):
                newarr[istep] = arr[istep-window:istep+window].mean()
            #Now I remove frequencies and signal from the point on where signal is 0
            for idx in range(0, len(freq), int(float(len(freq))/100)):
                if np.all(newarr[idx:]<1e-4):
                    return newarr[:idx], freq[:idx]

            return newarr, freq

        from scipy import signal
        from scipy.stats import sem



        # The time I consider the VAF to drop to 0 (diffusive regime) in fs:
        # n terms of a frequency, I need the inverse:
        freq_decorrelated = 0.05 * decorrelation_time_fs**(-1)
        # I know need to determine the filter window
        # Every
        #~ print freq_decorrelated

        #~ filter_window = 10

        self.fourier_velocities = []
        self.fourier_results = dict(decorrelation_time_fs=decorrelation_time_fs)
        for index_of_species, atomic_species in enumerate(self.species_of_interest):

            periodogram_this_species = []
            zero_freq_components = []
            for vel_array in self._velocities:
                pd_here = []
                fourier_vaf_this_species = []
                for idx in  self._get_indices_of_interest(atomic_species, start=0):
                    for ipol in range(3):
                        # Using signal periodogram to get the vib  signal:
                        freq, pd = signal.periodogram(vel_array[:, idx, ipol], fs=1./self.timestep_in_fs, return_onesided=True) # Show result in THz
                        # I have to order if return_onesided is False
                        #~ ordered = sorted(zip(freq, pd))
                        #~ freq, pd = (np.array(_) for _ in zip(*ordered))
                        # Why?
                        pd *= 0.5
                        #~ pd_filtered = signal.lfilter(1./float(filter_window)*np.ones(filter_window),1., pd)
                        pd_here.append(pd)

                counter = 0
                for frequency in freq:
                    if -freq_decorrelated < frequency < freq_decorrelated:
                        counter += 1
                filter_window = counter

                # I average over all my directions and trajectories:
                pd_mean = np.mean(np.array(pd_here), axis=0)
                #~ pd_filtered = signal.lfilter(1./float(filter_window)*np.ones(filter_window),1., pd_mean)
                # I filter to remove the big wiggles.
                pd_filtered, freq_filtered = myfilter(pd_mean, filter_window, freq)
                #~ pos_0 = pd_filtered.size/2

                #~ zero_freq_components.append(pd_filtered[pos_0])
                periodogram_this_species.append((freq_filtered, pd_filtered))
            self.fourier_velocities.append(periodogram_this_species)
            #~ D_mean = np.mean(zero_freq_components)
            #~ D_sem = sem(zero_freq_components)
            #~ self.fourier_results[atomic_species] = dict(
                    #~ zero_freq_components=zero_freq_components,
                    #~ zero_freq_components_mean=D_mean,
                    #~ zero_freq_components_sem=D_sem,
                    #~ D_mean_cm2_s=D_mean*0.1,
                    #~ D_sem_cm2_s=D_sem*0.1,
                #~ )

        return self.fourier_results, self.fourier_velocities


    def get_msd(self, **kwargs):
        """
        Calculates the mean square discplacement (MSD),
        Using the fortran module in lib.difflib.

        .. figure:: /images/fort_python_msd.pdf
            :figwidth: 100 %
            :width: 100 %
            :align: center

            Comparison between results achieved with the fortran implementation and python
            showing perfect agreement with respect to each other.

        Velocity autocorrelation function (VAF) and conductivity from the trajectory
        passed in :func:`set_trajectory`
        and the structure set in :func:`set_structure`.
        This procedes in several steps:

        #.  Calculate the MSD for each block, calculate the slope from the VAF0 estimate to block end
        #.  Calculate the mean and the standard deviation of the slope
        #.  Calculate the conductivity, including error propagation.

        :param list species_of_interest:
            The species of interest for which to calculate the MSD, for example ["O", "H"]
        :param int stepsize_t:
            This tells me whether I will have a stepsize larger than 1 (the default)
            when looping over the trajectory.
        """
        from difflib import calculate_msd_specific_atoms, calculate_msd_specific_atoms_decompose_d, get_com_positions
        try:

            #~ trajectory = self.trajectory
            timestep_in_fs = self.timestep_in_fs

        except AttributeError as e:
            raise Exception(
                "\n\n\n"
                "Please use the set_trajectories method to set trajectories"
                "\n{}\n".format(e)
            )
        try:
            structure = self.structure
            self._chemical_symbols
            species_of_interest = self.species_of_interest
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        log = self.log # set in __init__

        # Getting parameters for calculations

        #~ recenter = kwargs.pop('recenter', False)
        # Here I give the possibility to override species of interest just for
        # this calculation
        species_of_interest = kwargs.pop("species_of_interest", species_of_interest)

        stepsize_t  = kwargs.pop('stepsize_t', 1)
        stepsize_tau  = kwargs.pop('stepsize_tau', 1)

        t_start_fit_fs = kwargs.pop('t_start_fit_fs', None)
        t_start_fit_dt = kwargs.pop('t_start_fit_dt', None)


        if t_start_fit_fs and t_start_fit_dt:
            raise Exception("You cannot set both 't_start_fit_fs' and 't_start_fit_dt'")
        elif isinstance(t_start_fit_fs, (float, int)):
            t_start_fit_dt  = int(float(t_start_fit_fs) / timestep_in_fs)
        elif isinstance(t_start_fit_dt, int):
            block_length_fs  = t_start_fit_dt * timestep_in_fs
        else:
            raise Exception(
                "Please set the time to start the fit with keyword t_start_fit_fs or t_start_fit_dt"
            )


        t_start_msd_fs = kwargs.pop('t_start_msd_fs', None)
        t_start_msd_dt = kwargs.pop('t_start_msd_dt', 0)

        if isinstance(t_start_msd_fs, float):
            t_start_msd_dt = int(t_start_msd_fs / timestep_in_fs)
        elif isinstance(t_start_msd_dt, int):
            t_start_msd_fs = t_start_msd_dt*timestep_in_fs
        else:
            raise Exception(
                "\n\n\n"
                "Set the time that you consider the\n"
                "VAF to have converged as a float with\n"
                "keyword t_start_msd_fs or t_start_msd_dt"
                "\n\n\n"
            )



        t_end_fit_fs = kwargs.pop('t_end_fit_fs', None)
        t_end_fit_dt = kwargs.pop('t_end_fit_dt', None)

        if t_end_fit_fs and t_end_fit_dt:
            raise Exception("You cannot set both 't_end_fit_fs' and 't_end_fit_dt'")
        elif isinstance(t_end_fit_fs, (int,float)):
            t_end_fit_dt  = int(float(t_end_fit_fs) / timestep_in_fs)
        elif isinstance(t_end_fit_dt, int):
            block_length_fs  = t_end_fit_dt * timestep_in_fs
        else:
            raise Exception(
                "Please set the time to start the fit with keyword t_end_fit_fs or t_end_fit_dt"
            )


        t_end_msd_fs = kwargs.pop('t_end_msd_fs', None)
        t_end_msd_dt = kwargs.pop('t_end_msd_dt', None)

        if t_end_msd_fs and t_end_msd_dt:
            raise Exception("You cannot set both 't_end_msd_fs' and 't_end_msd_dt'")
        if isinstance(t_end_msd_fs, float):
            t_end_msd_dt = int(t_end_msd_fs / timestep_in_fs)
        elif isinstance(t_end_msd_dt, int):
            t_end_msd_fs = timestep_in_fs * t_end_msd_dt
        else:
            t_end_msd_dt = t_end_fit_dt
            #~ t_end_msd_fs = t_end_fit_dt

        # The number of timesteps I will calculate:
        nr_of_t = (t_end_msd_dt - t_start_msd_dt) / stepsize_t # In principle I could start at t_start_msd_dt, but for the
        # the plotting I will calculate also the part between 0 adn t_start.

        # Checking if I have to partition the trajectory into blocks (By default just 1 block)
        block_length_fs = kwargs.pop('block_length_fs', None)
        block_length_dt = kwargs.pop('block_length_dt', None)

        nr_of_blocks = kwargs.pop('nr_of_blocks', None)


        # Asking whether to calculate COM diffusion
        do_com = kwargs.pop('do_com', False)


        if kwargs:
            raise Exception("Uncrecognized keywords: {}".format(kwargs.keys()))
        if block_length_fs and block_length_dt :
           raise Exception("You cannot set both 'block_length_fs' and 'block_length_dt'")
        elif ( block_length_dt or block_length_fs ) and nr_of_blocks:
            raise Exception("You cannot set both 'a block length' and 'nr_of_blocks'") #
            # Maybe allow this in the future, at the cost of not the whole trajectory being used:
        elif isinstance(block_length_fs, float):
            block_length_dt  = int(block_length_fs / timestep_in_fs)
        elif isinstance(block_length_dt, int):
            block_length_fs  = block_length_dt * timestep_in_fs
        elif isinstance(nr_of_blocks, int):
            nr_of_blocks = nr_of_blocks
        else:
            nr_of_blocks=1
            #~ raise Exception(
                #~ "Please set the block length in femtoseconds (kw block_length_fs or block_length_dt) or define the\n"
                #~ "number of blocks (kw nr_of_blocks)"
            #~ )

        #~ assert nr_of_blocks > 0, 'Number of blocks is not positive'


        # Make this not overwrite results within the loop:
        self.msd_results_dict = {atomic_species: {}
                for atomic_species
                in species_of_interest
            }
        self.msd_isotrop_all_species = []
        self.msd_averaged = []
            # Setting params for calculation of MSD and conductivity
            # Future: Maybe allow for element specific parameter settings?

        for atomic_species in species_of_interest:

            if do_com:
                indices_of_interest = [1]
                prefactor = len(self._get_indices_of_interest(atomic_species, start=0))
            else:
                indices_of_interest = self._get_indices_of_interest(atomic_species, start=1)
                prefactor = 1

            nat_of_interest = len(indices_of_interest)

            msd_isotrop_this_species = []

            slopes = []
            intercepts = []
            labels = []

            for itraj, trajectory in enumerate(self._positions, start=1):

                nstep, nat, _= trajectory.shape
                total_time = nstep*self.timestep_in_fs

                if nr_of_blocks:
                    block_length_dt = (nstep -t_end_msd_dt)  / nr_of_blocks
                    block_length_fs = block_length_dt*timestep_in_fs
                else:
                    nr_of_blocks   = (nstep - t_end_msd_dt) / block_length_dt

                if do_com:
                    factors = self._get_factors(atomic_species)
                    trajectory = get_com_positions(trajectory, self._masses, factors, nstep, nat)
                    nstep, nat, _= trajectory.shape

                if self._verbosity > 0:
                    log.write(
                        '\n    ! Calculating MSD for atomic species {} in trajectory {}\n'
                        '      Structure contains {} atoms of type {}\n'
                        '      Assuming convergence of VAF at {}\n'
                        '      Block length for calculation is {} fs ({} dt)\n'
                        '      I will calculate {} block(s)\n'
                        ''.format(
                            atomic_species, itraj, nat_of_interest, atomic_species,
                            t_start_msd_fs, block_length_fs, block_length_dt, nr_of_blocks
                        )
                    )

                msd_isotrop_this_species_this_traj = prefactor*calculate_msd_specific_atoms(
                            trajectory,
                            indices_of_interest,
                            stepsize_t,
                            stepsize_tau,
                            block_length_dt,
                            nr_of_blocks,
                            nr_of_t,
                            nstep,
                            nat,
                            nat_of_interest
                        )

                if self._verbosity > 0:
                    log.write('      Done\n')
                range_for_t = timestep_in_fs*stepsize_t*np.arange(t_start_fit_dt/stepsize_t, t_end_fit_dt/stepsize_t)

                for iblock, block in enumerate(msd_isotrop_this_species_this_traj, start=1):
                    slope, intercept, _, _, _ = linregress(range_for_t, block[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t])
                    slopes.append(slope)
                    intercepts.append(intercept)
                    msd_isotrop_this_species.append(block)
                    labels.append('{}, T:{}, B:{}'.format(atomic_species, itraj, iblock))


            # Calculating the average sem/std for each point in time:
            msd_mean = np.mean(msd_isotrop_this_species, axis=0)
            msd_std = np.std(msd_isotrop_this_species, axis=0)
            msd_sem = msd_std / np.sqrt(len(msd_isotrop_this_species) - 1)
            upper_bound_sem_slope,_,_,_,_ = linregress(range_for_t, (msd_mean+msd_sem)[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t])
            lower_bound_sem_slope,_,_,_,_ = linregress(range_for_t, (msd_mean-msd_sem)[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t])
            upper_bound_std_slope,_,_,_,_ = linregress(range_for_t, (msd_mean+msd_std)[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t])
            lower_bound_std_slope,_,_,_,_ = linregress(range_for_t, (msd_mean-msd_std)[t_start_fit_dt/stepsize_t:t_end_fit_dt/stepsize_t])

            # The factor 1e-5 comes from the conversion of A**2 / fs -> m**2/ s
            self.msd_averaged.append((
                    msd_mean, msd_sem, msd_std,
                    1e-5 / 6.* 0.5*(upper_bound_sem_slope+lower_bound_sem_slope), # For convenience I put here the mean and sem that I calculated for the diffusion
                    1e-5 / 6.* 0.5*(upper_bound_sem_slope-lower_bound_sem_slope),
                    1e-5 / 6.* 0.5*(upper_bound_std_slope+lower_bound_std_slope), # For convenience I put here the mean and sem that I calculated for the diffusion
                    1e-5 / 6.* 0.5*(upper_bound_std_slope-lower_bound_std_slope),
                ))

            self.msd_results_dict[atomic_species].update({
                'slope_msd_mean':np.mean(slopes),
                'slope_msd_std':np.std(slopes),
                'slope_msd_sem':standard_error_of_mean(slopes),
                'slopes_n_intercepts':zip(slopes,intercepts),
                'labels':labels,
            })
            diffusion_mean_SI = 1e-5 / 6.* self.msd_results_dict[atomic_species]['slope_msd_mean']
            diffusion_std_SI = 1e-5 / 6.* self.msd_results_dict[atomic_species]['slope_msd_std']
            diffusion_sem_SI = 1e-5 / 6.* self.msd_results_dict[atomic_species]['slope_msd_sem']

            self.msd_results_dict[atomic_species]['diffusion_mean_SI'] =  diffusion_mean_SI
            self.msd_results_dict[atomic_species]['diffusion_std_SI']  =  diffusion_std_SI
            self.msd_results_dict[atomic_species]['diffusion_sem_SI']  =  diffusion_sem_SI
            self.msd_results_dict[atomic_species]['diffusion_mean_cm2_s'] =  1e4*diffusion_mean_SI
            self.msd_results_dict[atomic_species]['diffusion_std_cm2_s']  =  1e4*diffusion_std_SI
            self.msd_results_dict[atomic_species]['diffusion_sem_cm2_s']  =  1e4*diffusion_sem_SI


            if self._verbosity > 0:
                log.write(
                    '      Done, these are the results for {}'
                    '\n'.format(atomic_species)
                )
                [
                    log.write(  '          {:<20} {}\n'.format(key,  val))
                    for key, val in self.msd_results_dict[atomic_species].items()
                    if not isinstance(val, (tuple, list, dict))
                ]

            self.msd_isotrop_all_species.append(msd_isotrop_this_species)

        self.msd_results_dict.update({
            't_start_fit_dt'        :   t_start_fit_dt,
            't_end_fit_dt'          :   t_end_fit_dt,
            't_start_msd_dt'        :   t_start_msd_dt,
            't_end_msd_dt'          :   t_end_msd_dt,
            'nr_of_blocks'          :   nr_of_blocks,
            'block_length_dt'       :   block_length_dt,
            'stepsize_t'            :   stepsize_t,
            'species_of_interest'   :   species_of_interest,
            'timestep_in_fs'        :   timestep_in_fs,
            'nr_of_t'               :   nr_of_t,
        })

        return self.msd_results_dict, np.array(self.msd_isotrop_all_species)

    def get_msd_decomposed(self, **kwargs):
        """
        Calculates the mean square discplacement (MSD),
        Using the fortran module in lib.difflib.

        .. figure:: /images/fort_python_msd.pdf
            :figwidth: 100 %
            :width: 100 %
            :align: center

            Comparison between results achieved with the fortran implementation and python
            showing perfect agreement with respect to each other.

        Velocity autocorrelation function (VAF) and conductivity from the trajectory
        passed in :func:`set_trajectory`
        and the structure set in :func:`set_structure`.
        This procedes in several steps:

        #.  Calculate the MSD for each block, calculate the slope from the VAF0 estimate to block end
        #.  Calculate the mean and the standard deviation of the slope
        #.  Calculate the conductivity, including error propagation.

        :param list species_of_interest:
            The species of interest for which to calculate the MSD, for example ["O", "H"]
        :param int stepsize:
            This tells me whether I will have a stepsize larger than 1 (the default)
            when looping over the trajectory. I.e. when set to 10, will consider every
            10th timestep of the trajectory
        """
        from difflib import calculate_msd_specific_atoms_decompose_d
        try:

            #~ trajectory = self.trajectory
            timestep_in_fs = self.timestep_in_fs

        except AttributeError as e:
            raise Exception(
                "\n\n\n"
                "Please use the set_trajectories method to set trajectories"
                "\n{}\n".format(e)
            )
        try:
            structure = self.structure
            self._chemical_symbols
            species_of_interest = self.species_of_interest
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        log = self.log # set in __init__


        # Getting parameters for calculations

        #~ recenter = kwargs.pop('recenter', False)

        # Here I give the possibility to override species of interest just for
        # this calculation
        species_of_interest = kwargs.pop("species_of_interest", species_of_interest)
        stepsize  = kwargs.pop('stepsize', 1)
        stepsize_tau  = kwargs.pop('stepsize_tau', 1)

        t_start_fit_fs = kwargs.pop('t_start_fit_fs', None)
        t_start_fit_dt = kwargs.pop('t_start_fit_dt', None)

        if t_start_fit_fs and t_start_fit_dt:
            raise Exception("You cannot set both 't_start_fit_fs' and 't_start_fit_dt'")
        elif isinstance(t_start_fit_fs, float):
            t_start_fit_dt  = int(t_start_fit_fs / timestep_in_fs)
        elif isinstance(t_start_fit_dt, int):
            block_length_fs  = t_start_fit_dt * timestep_in_fs
        else:
            raise Exception(
                "Please set the time to start the fit with keyword t_start_fit_fs or t_start_fit_dt"
            )

        only_means = kwargs.pop('only_means', False)

        t_start_msd_fs = kwargs.pop('t_start_msd_fs', None)
        t_start_msd_dt = kwargs.pop('t_start_msd_dt', 0)

        if isinstance(t_start_msd_fs, float):
            t_start_msd_dt = int(t_start_msd_fs / timestep_in_fs)
        elif isinstance(t_start_msd_dt, int):
            t_start_msd_fs = t_start_msd_dt*timestep_in_fs
        else:
            raise Exception(
                "\n\n\n"
                "Set the time that you consider the\n"
                "VAF to have converged as a float with\n"
                "keyword t_start_msd_fs or t_start_msd_dt"
                "\n\n\n"
            )



        t_end_fit_fs = kwargs.pop('t_end_fit_fs', None)
        t_end_fit_dt = kwargs.pop('t_end_fit_dt', None)

        if t_end_fit_fs and t_end_fit_dt:
            raise Exception("You cannot set both 't_end_fit_fs' and 't_end_fit_dt'")
        elif isinstance(t_end_fit_fs, float):
            t_end_fit_dt  = int(t_end_fit_fs / timestep_in_fs)
        elif isinstance(t_end_fit_dt, int):
            block_length_fs  = t_end_fit_dt * timestep_in_fs
        else:
            raise Exception(
                "Please set the time to start the fit with keyword t_end_fit_fs or t_end_fit_dt"
            )


        t_end_msd_fs = kwargs.pop('t_end_msd_fs', None)
        t_end_msd_dt = kwargs.pop('t_end_msd_dt', None)

        if t_end_msd_fs and t_end_msd_dt:
            raise Exception("You cannot set both 't_end_msd_fs' and 't_end_msd_dt'")
        if isinstance(t_end_msd_fs, float):
            t_end_msd_dt = int(t_end_msd_fs / timestep_in_fs)
        elif isinstance(t_end_msd_dt, int):
            t_end_msd_fs = timestep_in_fs * t_end_msd_dt
        else:
            t_end_msd_dt = t_end_fit_dt
            #~ t_end_msd_fs = t_end_fit_dt

        # The number of timesteps I will calculate:
        nr_of_t = (t_end_msd_dt - t_start_msd_dt) / stepsize # In principle I could start at t_start_msd_dt, but for the
        # the plotting I will calculate also the part between 0 adn t_start.

        # Checking if I have to partition the trajectory into blocks (By default just 1 block)
        block_length_fs = kwargs.pop('block_length_fs', None)
        block_length_dt = kwargs.pop('block_length_dt', None)

        nr_of_blocks = kwargs.pop('nr_of_blocks', None)

        if kwargs:
            raise Exception("Uncrecognized keywords: {}".format(kwargs.keys()))
        if block_length_fs and block_length_dt :
           raise Exception("You cannot set both 'block_length_fs' and 'block_length_dt'")
        elif ( block_length_dt or block_length_fs ) and nr_of_blocks:
            raise Exception("You cannot set both 'a block length' and 'nr_of_blocks'") #
            # Maybe allow this in the future, at the cost of not the whole trajectory being used:
        elif isinstance(block_length_fs, float):
            block_length_dt  = int(block_length_fs / timestep_in_fs)
        elif isinstance(block_length_dt, int):
            block_length_fs  = block_length_dt * timestep_in_fs
        elif isinstance(nr_of_blocks, int):
            nr_of_blocks = nr_of_blocks
        else:
            nr_of_blocks=1
            #~ raise Exception(
                #~ "Please set the block length in femtoseconds (kw block_length_fs or block_length_dt) or define the\n"
                #~ "number of blocks (kw nr_of_blocks)"
            #~ )


        assert nr_of_blocks > 0, 'Number of blocks is not positive'


        # Make this not overwrite results within the loop:
        self.msd_results_dict_decomposed = {atomic_species: {}
                for atomic_species
                in species_of_interest
            }

        self.msd_decomposed = [] # That's where I'm storing the MSD decomposed as a function of time.
        # Each MSD is a 3x3xT matrix, T is time! 3x3 from the 3 coordinates



        for ityp, atomic_species in enumerate(species_of_interest):

            self.msd_results_dict_decomposed[atomic_species]['slopes'] = []
            self.msd_results_dict_decomposed[atomic_species]['intercepts'] = []
            #~ self.msd_results_dict_decomposed[atomic_species]['blocks'] = []
            #~ self.msd_results_dict_decomposed[atomic_species]['diffusions'] = []
            #~ self.msd_results_dict_decomposed[atomic_species]['diffusion_mean'] = []
            #~ self.msd_results_dict_decomposed[atomic_species]['diffusion_std'] = []
            #~ self.msd_results_dict_decomposed[atomic_species]['diffusion_sem'] = []
            #~ self.msd_results_dict_decomposed[atomic_species]['labels'] = []

            indices_of_interest = self._get_indices_of_interest(atomic_species, start=1)

            nat_of_interest = len(indices_of_interest)

            msd_decomposed_this_species = []

            ntraj = len(self._positions)

            # Here I'm storing the slopes of each block over all trajectories:
            slopes = []

            for itraj, trajectory in enumerate(self._positions):

                nstep, nat, _= trajectory.shape
                total_time = nstep*self.timestep_in_fs
                if nr_of_blocks:
                    block_length_dt = (nstep -t_end_msd_dt)  / nr_of_blocks
                    block_length_fs = block_length_dt*timestep_in_fs
                else:
                    nr_of_blocks   = (nstep -max_time_in_dt) / block_length_dt


                #~ labels_dec_m = np.chararray((nr_of_blocks, 3,3), itemsize=25)
                slope_dec_m = np.empty((nr_of_blocks, 3,3))
                intercept_dec_m = np.empty((nr_of_blocks, 3,3))
                #~ diffusion_dec_m = np.empty((nr_of_blocks, 3,3))
                #~ diffusion_dec_mean = np.empty((3,3))
                #~ diffusion_dec_std = np.empty((3,3))
                #~ diffusion_dec_sem = np.empty((3,3))
                #~ block_dec_m = np.empty((nr_of_blocks, nr_of_t, 3,3))


                log.write(
                    '\n    ! Calculating MSD for atomic species {} in trajectory {}\n'
                    '      Structure contains {} atoms of type {}\n'
                    '      Assuming convergence of VAF at {}\n'
                    '      Block length for calculation is {} fs ({} dt)\n'
                    '      I will calculate {} block(s)\n'
                    ''.format(
                        atomic_species, itraj, nat_of_interest, atomic_species,
                        t_start_msd_fs, block_length_fs, block_length_dt, nr_of_blocks
                    )
                )

                msd_decomposed_this_species_this_traj = calculate_msd_specific_atoms_decompose_d(
                        trajectory,
                        indices_of_interest,
                        stepsize,
                        stepsize_tau,
                        block_length_dt,
                        nr_of_blocks,
                        nr_of_t,
                        nstep,
                        nat,
                        nat_of_interest
                    )
                log.write('      Done\n')
                range_for_t = timestep_in_fs*stepsize*np.arange(t_start_fit_dt/stepsize, t_end_fit_dt/stepsize)

                # Storing the MSD decomposed as a function of time here:
                msd_decomposed_this_species.append(msd_decomposed_this_species_this_traj)


                for iblock, block in enumerate(msd_decomposed_this_species_this_traj):
                    for ipol in range(3):
                        for jpol in range(3):
                            slope, intercept, _, _, _ = linregress(range_for_t, block[t_start_fit_dt/stepsize:t_end_fit_dt/stepsize,ipol, jpol])
                            slope_dec_m[iblock, ipol, jpol] = slope
                            intercept_dec_m[iblock, ipol, jpol] = intercept

                self.msd_results_dict_decomposed[atomic_species]['slopes'].append(slope_dec_m.tolist())
                self.msd_results_dict_decomposed[atomic_species]['intercepts'].append(intercept_dec_m.tolist())


            # Here I'm calculating the mean for all blocks and trajectories
            all_slopes = np.array(self.msd_results_dict_decomposed[atomic_species]['slopes'])
            if only_means:
                self.msd_results_dict_decomposed[atomic_species].pop('slopes')
                self.msd_results_dict_decomposed[atomic_species].pop('intercepts')
            #~ print 0.5e-1*all_slopes.mean(axis=0)[0]
            
            # 1e-1 to cgs units
            #~ print all_slopes[:,0,1,2].mean()
            
            slopes_dict = {
                'slope_msd_mean':all_slopes.mean(axis=0)[0],
                'slope_msd_std':all_slopes.std(axis=0)[0],
                'slope_msd_sem':standard_error_of_mean(all_slopes, axis=0)[0],
            }
            #~ print slopes_dict['slope_msd_std']
            #~ print slopes_dict['slope_msd_sem']
            diffusion_dict = dict(
                diffusion_mean_cm2_s = 0.5e-1*slopes_dict['slope_msd_mean'],
                diffusion_std_cm2_s = 0.5e-1*slopes_dict['slope_msd_std'],
                diffusion_sem_cm2_s = 0.5e-1*slopes_dict['slope_msd_sem'],
            )

            for d in (slopes_dict, diffusion_dict):
                self.msd_results_dict_decomposed[atomic_species].update({k:v.tolist() for k,v in d.items()})


            self.msd_decomposed.append(msd_decomposed_this_species)
        self.msd_results_dict_decomposed.update({
            't_start_fit_dt'        :   t_start_fit_dt,
            't_end_fit_dt'          :   t_end_fit_dt,
            't_start_msd_dt'        :   t_start_msd_dt,
            't_end_msd_dt'          :   t_end_msd_dt,
            'nr_of_blocks'          :   nr_of_blocks,
            'block_length_dt'       :   block_length_dt,
            'stepsize'              :   stepsize,
            'species_of_interest'   :   species_of_interest,
            'timestep_in_fs'        :   timestep_in_fs,
            'nr_of_t'               :   nr_of_t,
            'ntraj'                 :   itraj+1,
        })
        return self.msd_results_dict_decomposed, np.array(self.msd_decomposed)





    def calculate_conductivity_msd(self):

        try:
            temperature=self.temperature
        except AttributeError:
            raise Exception("Set the temperature  using set_temperature")
        volume = calc_cell_volume(self.structure.cell)

        self.conductivity_results = {'temperature':self.temperature, 'volume':volume}
        for atomic_species in self.msd_results_dict['species_of_interest']:
            (conductivity_mean, conductivity_std, conductivity_sem) = nernst_einstein_eq(
                    self.msd_results_dict[atomic_species]['diffusion_mean_SI'],
                    self.msd_results_dict[atomic_species]['diffusion_std_SI'],
                    self.msd_results_dict[atomic_species]['diffusion_sem_SI'],
                    temperature=temperature,
                    volume=volume,
                    nr_of_ions=self._chemical_symbols.count(atomic_species)
                )
            resdict = {
                'conductivity_mean_SI': conductivity_mean,
                'conductivity_std_SI':  conductivity_std,
                'conductivity_sem_SI':  conductivity_sem,
                'diffusion_mean_SI'  :  self.msd_results_dict[atomic_species]['diffusion_mean_SI'],
                'diffusion_std_SI'   :  self.msd_results_dict[atomic_species]['diffusion_std_SI'],
                'diffusion_sem_SI'   :  self.msd_results_dict[atomic_species]['diffusion_sem_SI'],
            }

            if self._verbosity > 0:
                print "   calculating diffusion and conductivity for", atomic_species
                for k,v in resdict.items():
                    print '      {:<20} | {}'.format(k,v)

            self.conductivity_results[atomic_species] = resdict
        return self.conductivity_results

    def set_msd_results(self, res, array=None):
        if array is None:
            self.msd_isotrop_all_species = res.pop('array')
        else:
            self.msd_isotrop_all_species=array
        self.msd_results_dict = res

    def set_msd_decomposed_results(self, res, array=None):
        if array is None:
            self.msd_decomposed = res.pop('array')
        else:
            self.msd_decomposed=array
        self.msd_results_dict_decomposed = res

    def set_vaf_results(self, res, array=None):
        if array is None:
            self.vaf_all_species = res.pop('array')
        else:
            self.vaf_all_species = array
        self.vaf_results_dict = res


    def calculate_kinE(self, stepsize, decompose_atoms=False, decompose_species=False):

        self._kine_stepsize = stepsize
        prefactor = amu_kg* 1e10 / kB
        # * 1.06657254018667
        if not self._velocities:
            print "HERE"
            raise Exception("I don't have velocities")
        if decompose_atoms and decompose_species:
            raise Exception("Cannot decompose atoms and decompose species")

        self._kinetics_decomposed_atoms = decompose_atoms
        if decompose_atoms:
            self.kinetic_energies_d_atoms = []

            for vel_array in self._velocities:
                nstep, nat, _ = vel_array.shape

                steps = list(range(0, nstep, stepsize))
                kinetic_energy_atom_step = np.zeros((len(steps),nat))

                #~ steps = list(range(0, nstep, stepsize))
                #~ kinE = np.empty((len(steps), nat))
                #~ kinE_step = np.empty(3)


                for istep0, istep in enumerate(steps):
                    #~ print istep0
                    for iat in xrange(nat):
                        for ipol in xrange(3):
                            kinetic_energy_atom_step[istep0, iat] += prefactor * self._masses[iat] * vel_array[istep, iat, ipol]**2 /3.
            self.kinetic_energies_d_atoms.append(kinetic_energy_atom_step)


        elif decompose_species:
            self.kinetic_energies_d_species = []
            self.mean_kinetic_energies_d_species = []

            for vel_array in self._velocities:
                nstep, nat, _ = vel_array.shape
                ntyp = len(self.species_of_interest)
                steps = list(range(0, nstep, stepsize))
                kinE_species = np.zeros((len(steps), ntyp))
                for ityp, atomic_species in enumerate(self.species_of_interest):
                    indices_of_interest = self._get_indices_of_interest(atomic_species, start=0)
                    for istep0, istep in enumerate(steps):
                        for idx, iat in enumerate(indices_of_interest):
                            for ipol in range(3):
                                kinE_species[istep0, ityp] += prefactor * self._masses[iat] * vel_array[istep, iat, ipol]**2

                    kinE_species[:,ityp] /= float(len(indices_of_interest)*3)
                self.kinetic_energies_d_species.append(kinE_species)
                self.mean_kinetic_energies_d_species.append(kinE_species.mean(axis=0))
            return self.mean_kinetic_energies_d_species
        else:
            self.kinetic_energies = []
            self.mean_kinetic_energies = []
            for vel_array in self._velocities:
                nstep, nat, _ = vel_array.shape

                steps = list(range(0, nstep, stepsize))
                #~ kinE = np.empty(len(steps))
                #~ kinE_step = np.empty((nat, 3))
                kinE = np.zeros(len(steps))

                for istep0, istep in enumerate(steps):
                    for iat in xrange(nat):
                        for ipol in xrange(3):
                            kinE[istep0] += prefactor * self._masses[iat] * vel_array[istep, iat, ipol]**2

                    # Dividing by the degrees of freedom!
                kinE[:] /= nat*3
                self.kinetic_energies.append(kinE)
                self.mean_kinetic_energies.append(kinE.mean())
            return self.mean_kinetic_energies


    def is_boltzmann_distributed(self, temperature, species_of_interest='Li', timesteps=None, bins=100):
        from scipy.stats import norm, normaltest
        #~ my_kB = kB_ev / hartree_to_ev #8.6173303e-5 / (hartree_to_ev*1822.888) * (bohr_to_ang / timeau_to_fs)**2
        indices = self._get_indices_of_interest(species_of_interest, start=0)
        masses = set(self._masses[indices])
        mass = masses.pop()*amu_au

        if masses:
            raise Exception("Many masses not implemented")
        sigma = np.sqrt(kB_au*temperature/mass)
        #~ timesteps = map(int, 16000*np.arange(1./16.,1.+1./16,1./16.))
        for vels in self._velocities:
            #~ efor idim in range(3):
            if timesteps is None:
                vel_interested_atomic = (vels[:,indices,:] / bohr_to_ang * timeau_to_fs).flatten()
            else:
                vel_interested_atomic = np.array([vels[t,indices,:] / bohr_to_ang * timeau_to_fs for t in timesteps]).flatten()
            P, bins = np.histogram(vel_interested_atomic, bins=bins, normed=1)
            V = 0.5*(bins[0:len(bins)-1] + bins[1:len(bins)])
            self.target_distribution = norm(0, sigma).pdf(V)
            self.velocity_distribution = V, P
            z, pval = normaltest(vel_interested_atomic)
            print z, pval

    def plot_results(
            self, label_blocks=False, title=None, plot_trajectory=False, plot_integral=True,
            block_frame=True, savefig=None, figsize=(13, 8), nolegend=False, color_by_tag=False,
            tag_color_dict=None, tag_marker_dict=None):
        """
        Plots the results of :func:`calculate_conductivity`

        .. figure:: /images/block_analysis_mp-685863_Li4Si1O4-temp-1000.pdf
            :figwidth: 100 %
            :width: 100 %
            :align: center

            An automatic trajectory analysis for mp-685863 at 1000K.
        """
        from matplotlib.ticker import FormatStrFormatter, ScalarFormatter
        nr_of_plots = 0
        try:
            vaf_all_species = np.array(self.vaf_all_species)
            timestep_in_fs = self.vaf_results_dict['timestep_in_fs']
            plot_vaf = True
            nr_of_plots+=1
        except AttributeError as e:
            #~ print e
            plot_vaf = False
        try:
            fourier_velocities = self.fourier_velocities
            plot_fourier = True
            nr_of_plots+=1
        except AttributeError:
            plot_fourier = False
        try:
            msd_all_species = self.msd_isotrop_all_species
            timestep_in_fs  = self.msd_results_dict['timestep_in_fs']
            plot_msd        = True
            nr_of_plots+=1
        except AttributeError as e:
            plot_msd        = False
        try:
            self.kinetic_energies
            plot_vel_temp=True
            nr_of_plots+=1
        except AttributeError:
            plot_vel_temp=False
        try:
            self.kinetic_energies_d_atoms
            plot_vel_temp_for_atoms=True
            nr_of_plots+=1
        except AttributeError:
            plot_vel_temp_for_atoms=False
        try:
            self.kinetic_energies_d_species
            plot_vel_temp_for_species=True
            nr_of_plots+=1
        except AttributeError:
            plot_vel_temp_for_species=False

        try:
            self.msd_results_dict_decomposed
            plot_msd_decomposed = True
            nr_of_plots+=1
        except AttributeError:
            plot_msd_decomposed = False
        try:
            self.velocity_distribution
            plot_boltzmann = True
            nr_of_plots+=1
        except:
            plot_boltzmann = False
        if plot_trajectory:
            nr_of_plots+=1



        if color_by_tag:
            if self._tags is None:
                raise Exception("You have to set tags")
            if tag_color_dict is None:
                tag_color_dict = {tag:np.random.random(3) for tag in self._tags}
            if tag_marker_dict is None:
                tag_marker_dict = {tag:'-' for tag in self._tags}

        gs = iter(GridSpec(nr_of_plots,1, hspace=0.6, left=0.1, right=0.85, bottom=0.1, top=0.9))

        cmap = plt.get_cmap('jet_r')
        fig = plt.figure(figsize=figsize)
        if title is not None:
            plt.suptitle(title, fontsize=16) # .format(timestep_in_fs / 1000 * total_steps)
        #~ else:
            #~ plt.suptitle('Analysis of MD-trajectory', fontsize=16) # .format(timestep_in_fs / 1000 * total_steps)
        f = ScalarFormatter()
        f.set_powerlimits((-1,1))


        if plot_trajectory:
            axes_traj = fig.add_subplot(gs.next())
            plt.xlabel('Time [ps]')
            plt.ylabel(r"q $[\AA]$")
            for index_of_species, atomic_species in enumerate(self.species_of_interest):
                indices_of_interest = self._get_indices_of_interest(atomic_species, start=0)
                color = jmol_colors[atomic_numbers[atomic_species]]
                for trajectory in self._positions:
                    nstep = trajectory.shape[0]
                    T = self.timestep_in_fs/1000.*np.arange(self._discard_dt, nstep+self._discard_dt)
                    axes_traj.plot(T, trajectory[:,indices_of_interest, 0], color=color, linestyle='-')
                    axes_traj.plot(T, trajectory[:,indices_of_interest, 1], color=color, linestyle='--')
                    axes_traj.plot(T, trajectory[:,indices_of_interest, 2], color=color, linestyle='-.')


        if plot_vel_temp:
            axes_kinE = fig.add_subplot(gs.next())
            plt.xlabel('Time [ps]')
            plt.ylabel('Temperature [K]')
            for idx, energy in enumerate(self.kinetic_energies):
                T = self.timestep_in_fs*self._kine_stepsize/1000.*np.arange(len(energy))
                axes_kinE.plot(T, energy, label='T={}'.format(self.mean_kinetic_energies[idx]))
                if 0:
                    with open("velocities_{}.txt".format(idx), 'w') as f:
                        for time, e in zip(T, energy):
                            f.write('{:12.10e}   {:12.10e}\n'.format(time, e))
            plt.legend()



        if plot_vel_temp_for_atoms:
            ax = fig.add_subplot(gs.next())
            plt.xlabel('Time [ps]')
            plt.ylabel('Temperature [K]')
            for energy in self.kinetic_energies_d_atoms:

                T = self.timestep_in_fs*self._kine_stepsize/1000.*np.arange(len(energy))

                for index_of_species, atomic_species in enumerate(self.species_of_interest):
                    indices_of_interest = self._get_indices_of_interest(atomic_species, start=0)
                    color = jmol_colors[atomic_numbers[atomic_species]]
                    for idx in indices_of_interest:
                        kinE_of_interest = energy[:, idx]
                        ax.plot(T, kinE_of_interest, color=color, label='T={}'.format(kinE_of_interest.mean()))
            plt.legend()

        if plot_vel_temp_for_species:
            ax = fig.add_subplot(gs.next())
            plt.xlabel('Time [ps]')
            plt.ylabel('Temperature [K]')
            for idx, energy in enumerate(self.kinetic_energies_d_species):
                T = self.timestep_in_fs*self._kine_stepsize/1000.*np.arange(len(energy))
                for index_of_species, atomic_species in enumerate(self.species_of_interest):
                    color = jmol_colors[atomic_numbers[atomic_species]]
                    kinE_of_interest = energy[:, index_of_species]
                    ax.plot(T, kinE_of_interest, color=color, label='T[{}]={:4.1f}'.format(atomic_species, self.mean_kinetic_energies_d_species[idx][index_of_species]))
            plt.legend()

        if plot_msd:
            nr_of_blocks      =  self.msd_results_dict['nr_of_blocks']
            block_length_dt   =  self.msd_results_dict['block_length_dt']
            t_start_fit_dt    =  self.msd_results_dict['t_start_fit_dt']
            stepsize          =  self.msd_results_dict.get('stepsize_t', 1)

            axes_msd = fig.add_subplot(gs.next())

            plt.ylabel(r'MSD $\left[ \AA^2 \right]$')
            plt.xlabel('Time $t$ [fs]')
            #~ plt.title('Block analysis of MSD')


            times_msd = timestep_in_fs*stepsize*np.arange(
                        self.msd_results_dict.get('t_start_msd_dt')/stepsize,
                        self.msd_results_dict.get('t_end_msd_dt')/stepsize
                    )

            times_fit =  timestep_in_fs*stepsize*np.arange(
                        self.msd_results_dict.get('t_start_fit_dt')/stepsize,
                        self.msd_results_dict.get('t_end_fit_dt')/stepsize
                    )

            for index_of_species, atomic_species in enumerate(
                    self.msd_results_dict['species_of_interest']
                ):

                diff = self.msd_results_dict[atomic_species]['diffusion_mean_cm2_s']
                diff_sem = self.msd_results_dict[atomic_species]['diffusion_sem_cm2_s']
                diff_std = self.msd_results_dict[atomic_species]['diffusion_std_cm2_s']

                #~ axes_msd.plot([],[], , color='w')
                #~ except Exception as e:
                    #~ print e
                color = jmol_colors[atomic_numbers[atomic_species]]
                axes_msd.plot([],[], color=color,
                    label=r'MSD ({})$\rightarrow D=( {:.2e} \pm {:.2e}) \frac{{cm^2}}{{s}}$'.format(atomic_species, diff, diff_sem))


                for block_index, block in enumerate(self.msd_isotrop_all_species[index_of_species]):
                    lower_bound = timestep_in_fs * block_index * block_length_dt
                    slope_this_block, intercept_this_block = self.msd_results_dict[atomic_species]['slopes_n_intercepts'][block_index]
                    if label_blocks:
                        label = self.msd_results_dict[atomic_species]['labels'][block_index]
                    else:
                        label = None

                    axes_msd.plot(
                            times_msd, block,
                            color=color,
                            label=label
                        )
                    axes_msd.plot(times_fit,
                            [slope_this_block*x+intercept_this_block for x in times_fit],
                            color=color, linestyle='--'
                        )
                    #~ print block[100],
                #~ print
                try:
                    (
                        msd_mean,  msd_sem, msd_std,
                        diff_from_sem_mean, diff_from_sem_err,
                        diff_from_std_mean, diff_from_std_err
                    ) = self.msd_averaged[index_of_species]

                    (
                        diff_from_sem_mean, diff_from_sem_err,
                        diff_from_std_mean, diff_from_std_err
                    ) = [1e4*v for v in  (
                            diff_from_sem_mean, diff_from_sem_err,
                            diff_from_std_mean, diff_from_std_err
                        )] # SI to cm2_s

                    plot_msd_averaged = True
                except AttributeError as e:
                    # If i was set frmo old results, I don't have this quantity!
                    plot_msd_averaged = False

                #~ print msd_mean[100],msd_sem[100],msd_std[100]
                # Filling with SEM


                if plot_msd_averaged:

                    p1 = axes_msd.fill_between(
                            times_msd, msd_mean-msd_sem, msd_mean+msd_sem,
                            facecolor='red', alpha=1,
                            linewidth=0,
                        )
                    # Patch to display fill between stuff:
                    #http://stackoverflow.com/questions/14534130/legend-not-showing-up-in-matplotlib-stacked-area-plot

                    # Filling with :
                    p2 = axes_msd.fill_between(
                            times_msd, msd_mean-2*msd_sem, msd_mean+2*msd_sem, facecolor='#FF7D00',
                            alpha=0.8, linewidth=0,
                        )

                    p2 = axes_msd.fill_between(
                            times_msd, msd_mean-3*msd_sem, msd_mean+3*msd_sem,facecolor='#FFFF00', alpha=0.5,
                            linewidth=0,
                        )


                if 0:

                    axes_msd.plot(
                            [],[], linewidth=10, alpha=0.3, color=p1.get_facecolor()[0],
                            label=r'$( {:.4g} \pm {:.4g}) \frac{{cm^2}}{{s}}$'.format(diff_from_sem_mean, diff_from_sem_err)
                        )

                    axes_msd.plot(
                            [],[], linewidth=10, alpha=0.3, color=p2.get_facecolor()[0],
                            label=r'$( {:.4g} \pm {:.4g}) \frac{{cm^2}}{{s}}$'.format(diff_from_std_mean, diff_from_std_err)
                        )


            if not(nolegend):
                leg = axes_msd.legend(loc=4)
                leg.get_frame().set_alpha(0.)


        if plot_msd_decomposed:

            nr_of_blocks      =  self.msd_results_dict_decomposed['nr_of_blocks']
            block_length_dt   =  self.msd_results_dict_decomposed['block_length_dt']
            t_start_fit_dt    =  self.msd_results_dict_decomposed['t_start_fit_dt']
            stepsize          =  self.msd_results_dict_decomposed['stepsize']
            timestep_in_fs    =  self.msd_results_dict_decomposed['timestep_in_fs']

            axes_msd_decomposed = fig.add_subplot(gs.next())

            plt.ylabel(r'MSD $\left[ \AA^2 \right]$')
            plt.xlabel('Time $t$ [fs]')
            #~ plt.title('Block analysis of MSD')

            times_msd = timestep_in_fs*stepsize*np.arange(
                        self.msd_results_dict_decomposed.get('t_start_msd_dt')/stepsize,
                        self.msd_results_dict_decomposed.get('t_end_msd_dt')/stepsize
                    )

            times_fit =  timestep_in_fs*stepsize*np.arange(
                        self.msd_results_dict_decomposed.get('t_start_fit_dt')/stepsize,
                        self.msd_results_dict_decomposed.get('t_end_fit_dt')/stepsize
                    )
            color_matrix = [['red', "#FAFA00", "#FA00FA"],
                            ["#FAFA00" ,'green', "#00FAFA"],
                            ["#FA00FA", "#00FAFA", 'blue']]

            for index_of_species, atomic_species in enumerate(self.msd_results_dict_decomposed['species_of_interest']):
                for itraj in range(self.msd_results_dict_decomposed['ntraj']):
                    for iblock in range(self.msd_results_dict_decomposed['nr_of_blocks']):
                        for ipol in range(3):
                            for jpol in range(3):
                                color = color_matrix[ipol][jpol]
                                try:
                                    slope_this_block_i_j = self.msd_results_dict_decomposed[atomic_species]['slopes'][itraj][iblock][ipol][jpol]
                                    intercept_this_block_i_j = self.msd_results_dict_decomposed[atomic_species]['intercepts'][itraj][iblock][ipol][jpol]

                                    axes_msd_decomposed.plot(times_fit, 
                                        [slope_this_block_i_j*x+intercept_this_block_i_j for x in times_fit],
                                        color=color, linestyle='--'
                                    )
                                except KeyError: # If I don't store the slopes and intercepts!
                                    pass

                                axes_msd_decomposed.plot(
                                    times_msd, self.msd_decomposed[index_of_species][itraj][iblock,:,ipol,jpol],
                                    color=color,
                                    #~ label=label
                                )

                for ipol, x1 in enumerate('XYZ'):
                    for jpol, x2 in enumerate('XYZ'[ipol:], start=ipol):
                        diff = self.msd_results_dict_decomposed[atomic_species]['diffusion_mean_cm2_s'][ipol][jpol]
                        diff_sem = self.msd_results_dict_decomposed[atomic_species]['diffusion_sem_cm2_s'][ipol][jpol]
                        color = color_matrix[ipol][jpol]
                        axes_msd_decomposed.plot([],[], color=color,
                            label=r'MSD ({})$\rightarrow D_{{{}{}}} =( {:.2e} \pm {:.2e}) \frac{{cm^2}}{{s}}$'.format(atomic_species, x1,x2, diff, diff_sem))
            leg = axes_msd_decomposed.legend(loc=2)
            leg.get_frame().set_alpha(0.)


        if plot_boltzmann:
            ax = fig.add_subplot(gs.next())
            V, P = self.velocity_distribution
            T = self.target_distribution

            ax.plot(V,P,  color='blue', label='measured distribution')
            ax.plot(V,T,  color='red', label='target distribution')

        if plot_vaf:


            axes_vaf = fig.add_subplot(gs.next())

            plt.ylabel(r'VAF $\left [\left(\frac{\AA}{fs}\right)^2 \right]$')
            plt.xlabel('Time $t$ [fs]')

            stepsize=self.vaf_results_dict['stepsize_t']

            times_vaf = timestep_in_fs*stepsize*np.arange(
                    self.vaf_results_dict.get('nr_of_t')
                )
            times_fit =  timestep_in_fs*stepsize*np.arange(
                self.vaf_results_dict['t_start_fit_dt']/stepsize,
                self.vaf_results_dict['t_end_fit_dt']/stepsize
            )
            axes_vaf.yaxis.set_major_formatter(f)
            if plot_integral:
                axes_D = axes_vaf.twinx()
                axes_D.yaxis.set_major_formatter(f)
                plt.ylabel(r"$\int^t_0 VAF(t') dt' \quad \left[ \frac{cm^2}{s} \right]$")

            for index_of_species, atomic_species in enumerate(
                    self.vaf_results_dict['species_of_interest']
                ):


                if atomic_species == 'H':
                    color_vaf = 'blue'
                    color_int = 'red'
                else:
                    color_vaf = jmol_colors[atomic_numbers[atomic_species]]
                    color_int = 1. - color_vaf

                for iblock, _ in enumerate(vaf_all_species[index_of_species]):
                    vaf_block, D = _

                    if color_by_tag:
                        color_vaf = tag_color_dict[self._tags[iblock]]


                    if label_blocks:
                        label1 = 'VAF {}'.format(atomic_species)
                        label2 = r"$D={:.4g} \frac{{cm^2}}{{s}}$".format(self.vaf_results_dict[atomic_species]['means_of_integral'][iblock])
                    else:
                        label1 = None
                        label2 = None

                    axes_vaf.plot(times_vaf, vaf_block, label= label1, color=color_vaf)
                    if plot_integral:
                        axes_D.plot(
                                times_vaf, D,
                                #~ linestyle='--',
                                label=label2, color=color_int)

                if not label_blocks:

                    plt.plot([], [], color=color_int, label=r'$\int VAF(t) dt \rightarrow D=({:.2e}\pm {:.2e}) \frac{{cm^2}}{{s}}$'.format(
                            self.vaf_results_dict[atomic_species]['means_of_means'],
                            self.vaf_results_dict[atomic_species]['sem_of_means']
                        ))
                    plt.plot([],[], label='VAF', color=color_vaf)

                if plot_integral:
                    (
                        D_mean, D_sem, D_std,
                        mean_from_sem, err_from_sem,
                        mean_from_std, err_from_std
                    ) = self.D_from_vaf_averaged[index_of_species]
                    #~ D_mean, D_sem


                    p1 = axes_D.fill_between(times_vaf, D_mean-D_sem, D_mean+D_sem, facecolor='red', alpha=1, linewidth=0)

                    #~ axes_D.plot(
                            #~ [],[], linewidth=10, alpha=0.3, color=p1.get_facecolor()[0],
                            #~ label=r'$( {:.4e} \pm {:.4e}) \frac{{cm^2}}{{s}}$'.format(mean_from_sem, err_from_sem)
                        #~ )

                    #~ p2 = axes_D.fill_between(times_vaf, D_mean-D_std, D_mean+D_std, facecolor='y', alpha=0.6, linewidth=0,)
                    p2 = axes_D.fill_between(times_vaf, D_mean-2*D_sem, D_mean+2*D_sem, facecolor='#FF7D00', alpha=0.8, linewidth=0,)
                    p3 = axes_D.fill_between(times_vaf, D_mean-3*D_sem, D_mean+3*D_sem, facecolor='#FFFF00', alpha=0.5, linewidth=0,)

                    #~ axes_D.plot(
                            #~ [],[], linewidth=10, alpha=0.3, color=p2.get_facecolor()[0],
                            #~ label=r'$( {:.4e} \pm {:.4e}) \frac{{cm^2}}{{s}}$'.format(mean_from_std, err_from_std   )
                        #~ )



                if label_blocks:
                    for slope, intercept in self.vaf_results_dict[atomic_species]['slopes_n_intercepts']:
                        plt.plot(times_fit, [slope*x+intercept for x in times_fit])

            if not(nolegend):
                leg = axes_D.legend(loc=1, ncol=1)
                leg.get_frame().set_alpha(0.)

        if plot_fourier:
            axes_fourier = fig.add_subplot(gs.next())
            axes_fourier.yaxis.set_major_formatter(f)
            axes_fourier.yaxis.tick_right()
            axes_fourier.yaxis.set_label_position("right")

            plt.xlabel(r'Frequency $[THz]$')
            plt.ylabel(r'Signal $\left[ 10^3 \cdot \AA^2 \cdot fs^{-1}\right]$')


            for index_of_species, atomic_species in enumerate(self.species_of_interest):
                color = jmol_colors[atomic_numbers[atomic_species]]
                idx=0
                for freq, signal in fourier_velocities[index_of_species]:
                    if color_by_tag:
                        color = tag_color_dict[self._tags[idx]]
                        linestyle = tag_marker_dict[self._tags[idx]]
                        label = self._tags[idx]
                    else:
                        label = None
                        linestyle = '-'
                    idx+=1
                    axes_fourier.plot(1e3*freq, 1e3*signal,color=color,
                            label=label, linestyle=linestyle,
                            linewidth=2
                    )
            #~ plt.legend(loc='upper right')


        #~ plt.legend(loc=2)

        if savefig:
            plt.savefig(savefig)
        else:
            plt.show(block=block_frame)

    def print_trajectories_of_interest(self, **kwargs):
        position_file_name = 'positions_'
        velocities_file_name = 'velocities_'
        for atom in self.species_of_interest:
            indices_of_interest=self._get_indices_of_interest(atom, start=0)

            for idx, pos_array in enumerate(self._positions, start=1):
                filename='{}{}_{}.xyz'.format(position_file_name, atom, idx)
                print 'Writing positions to', filename
                with open(filename, 'w') as f:
                    for istep, timestep in enumerate(pos_array[:,indices_of_interest,:]):
                        f.write('{}\n'.format(istep))
                        for xyz in timestep:
                            f.write('{:16.10f}   {:16.10f}   {:16.10f}\n'.format(*xyz))

            for idx, vel_array in enumerate(self._velocities, start=1):
                filename='{}{}_{}.xyz'.format(velocities_file_name, atom, idx)
                print 'Writing velocities to', filename
                with open(filename, 'w') as f:
                    for istep, timestep in enumerate(vel_array[:,indices_of_interest,:]):
                        f.write('{}\n'.format(istep))
                        for xyz in timestep:
                                f.write('{:16.10f}   {:16.10f}   {:16.10f}\n'.format(*xyz))

    def calculate_vaf_fort(self, **kwargs):
        """Calculates the velocities from the positions, use with care"""
        # HERE
        from lib import fortvaf
        try:
            trajectory     = self.trajectory
            timestep_in_fs = self.timestep_in_fs
            total_steps    = len(trajectory)
            total_time     = total_steps*self.timestep_in_fs
            nstep, nat, _  = trajectory.shape
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_trajectory method to set trajectory"
                "\n\n\n"
            )
        try:
            structure = self.structure
            self._chemical_symbols
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        log = self.log # set in __init__
        self.vaf_all_species =  []

        species_of_interest  = kwargs.get('species_of_interest', None)
        if species_of_interest is None:
            species_of_interest =  list(set(structure['atoms']))

        stepsize_t = kwargs.get('stepsize_t', 1)
        stepsize_tau = kwargs.get('stepsize_tau', 1)

        t_end_vaf_fs  = kwargs.get('t_end_vaf_fs', None)
        if not isinstance(t_end_vaf_fs, (int, float)):
            raise Exception(
                "\n\n\n"
                "Please set the time to calculate VAF for (in femtoseconds)"
                "\n\n\n"
            )

        t_end_vaf_dt  = int(t_end_vaf_fs / timestep_in_fs)
        nr_of_t        = t_end_vaf_dt / stepsize_t


        for atomic_species in species_of_interest:
            indices_of_interest = np.array(
                [
                    index
                    for index, atom
                    in enumerate(self._chemical_symbols)
                    if atom == atomic_species
                ],
                dtype='i'
            )
            # Python to fortran index i -> i+1
            indices_of_interest = indices_of_interest + 1
            nat_of_interest     = len(indices_of_interest)

            log.write(
                '\n    ! Calculating VAF for atomic species {}\n'
                '      Structure contains {} atoms of type {}\n'
                '      Max time (fs)     = {}\n'
                '      Max time (dt)     = {}\n'
                '      Stepsize for t    = {}\n'
                '      stepsize for tau  = {}\n'
                '      nr of timesteps   = {}\n'
                '      Calculating VAF with fortran subroutine fortvaf.calculate_vaf_specific_atoms\n'
                ''.format(
                    atomic_species,
                    nat_of_interest,
                    atomic_species,
                    t_end_vaf_fs,
                    t_end_vaf_dt,
                    stepsize_t,
                    stepsize_tau,
                    nr_of_t,
                )
            )


            vaf = fortvaf.calculate_vaf_specific_atoms(
                trajectory,
                indices_of_interest,
                stepsize_t,
                stepsize_tau,
                nr_of_t,
                nstep,
                nat,
                nat_of_interest
            )
            log.write(
                "      calculate_vaf_specific_atoms, returned array of length {}\n"
                "".format(len(vaf))
            )
            self.vaf_all_species.append(vaf)
        self.vaf_results_dict = dict(
            t_end_vaf_fs    =   t_end_vaf_fs,
            t_end_vaf_dt    =   t_end_vaf_dt,
            stepsize_t  =   stepsize_t,
            stepsize_tau    =   stepsize_tau,
            timestep_in_fs  =   timestep_in_fs,
            nr_of_t         =   nr_of_t,
            species_of_interest=species_of_interest,
        )
        return self.vaf_results_dict, np.array(self.vaf_all_species)


    def animate(self, save_animation=False, name=None):
        """
        View the trajectory in an animation, including the movement of the hulls.
        """
        from matplotlib import animation, colors, gridspec
        import mpl_toolkits.mplot3d.axes3d as p3
        from ase.data import covalent_radii
        def get_timestep(
                num, ax, ax2, trajectory, atoms,
                bar, progress_title
            ):
            timestep = trajectory[num]
            for indeks, atom in enumerate(atoms):
                position = [[i] for i in timestep[indeks]]
                atom.set_data(*position[:2])
                atom.set_3d_properties(position[2])
                pos_hack = [[i] for i in position]
                if indeks in tracklist:
                    ax.plot(
                            *position, marker='.',
                            markersize=3, color=atom.get_color()
                        )
            if num:
                bar.set_width(num)
            plt.setp(
                progress_title,
                text='progress: {:.2f}%, @Timestep: {} , @Time: {:.2f} fs'.format(
                    100.* num / len(positions),
                    num,
                    self.timestep_in_fs*num
                )
            )

            ############# END get_timestep ####################################

        tracklist = list()
        for species in self.species_of_interest:
            tracklist += set(self._get_indices_of_interest(species, 0))

        cmap = plt.cm.BuPu
        cNorm  = colors.Normalize(vmin=0, vmax=1)
        scalarMap = plt.cm.ScalarMappable(norm=cNorm, cmap=cmap)
        gs = gridspec.GridSpec(2,1, height_ratios = [50,1])


        for positions in self._positions:
            fig = plt.figure(facecolor='white', figsize = (16,9))

            ax = p3.Axes3D(fig)
            #~ ax = fig.add_subplot(gs[0], projection = '3D')
            ax2 = fig.add_subplot(gs[1])

            if name is not None:
                plt.suptitle(name, fontsize = 20)
            ax.set_axis_off()
            # draw the cell
            pp = [[i,j,k] for i in range(2) for j in range(2) for k in range(2)]
            for p1 in pp:
                for p2 in pp:
                    #skip unnecessary points:
                    counts = [p1[i] == p2[i] for i in range(3)]
                    if not counts.count(True) == 2:
                        continue
                    ax.plot(
                        *zip(
                                np.dot([p1], self.structure.cell).tolist()[0],
                                np.dot([p2], self.structure.cell).tolist()[0]
                            ),
                        color=(0,0,0)
                    )

            # list atoms is the spheres in the visualization, dont confuse with
            # self.atoms, the ASE instance
            atoms = []
            label_dict = {}
            for id_tracked in tracklist:
                ele = self.structure[id_tracked].symbol
                pos = self.structure[id_tracked].position
                atomic_number = atomic_numbers[ele]
                color = jmol_colors[atomic_number]
                size_to_give = 20. * covalent_radii[atomic_number]
                handle = ax.plot(
                        *zip(pos), marker='o', #linestyle='o',
                        color=color, markersize=size_to_give
                    )[0]
                atoms.append(handle)
                label_dict.update({ele: handle})
            if 0:
                tetrahedra = []
                for index, node in enumerate(self.nodes):
                    try:
                        points = list(node.cage_positions)
                    except:
                        node.initialize_hull()
                        points = list(node.cage_positions)
                    assert len(points) == 4
                    simplices = ConvexHull(points).simplices
                    poly3d = [
                            [
                                points[simplices[ix][iy]]
                                for iy
                                in range(len(simplices[0]))
                            ]
                            for ix
                            in range(len(simplices))
                        ]
                    collection = Poly3DCollection(poly3d, linewidths=0.015, alpha=0.00)
                    face_color = 'white' #scalarMap.to_rgba(np.random.random())
                    collection.set_facecolor(face_color)
                    ax.add_collection3d(collection)
                    tetrahedra.append(collection)
            ax.legend(
                    label_dict.values(), label_dict.keys(),
                    frameon=False, loc=3, numpoints=1,
                    ncol=len(label_dict)
                )

            bar = ax2.barh([0.2], [1], color = 'green')[0]

            plt.xticks(range(0, len(positions), len(positions)/10))
            plt.yticks([])
            ax2.set_axis_off()
            progress_title = plt.title('Progress', fontsize=15)
            ax.autoscale(False)

            fps = 1

            #~ return
            timesteps_to_play = 30*fps if save_animation else len(positions)
            ani = animation.FuncAnimation(
                    fig, get_timestep, timesteps_to_play,
                    fargs=(
                            ax, ax2, positions,
                            atoms,
                            # tetrahedra,
                            bar,
                            progress_title
                        ),
                    blit=False
                )

        if save_animation:
            folder = '{}/Videos/videos_pub/lithium_animations'.format(os.getenv('HOME'))
            i = 1
            while True:
                filepath = '{}/{}_{}.mp4'.format(folder, save_animation, i)
                if not os.path.exists(filepath):
                    break
                i +=1
            print 'writing to', filepath
            ani.save(filepath, fps=fps, dpi = 200)
        else:
            plt.show()



def get_vaf_of_t(args):
    """
    Get velocity autocorelation function for single time t averaged voer trajectory
    """
    t, velocities_all_atoms = args
    range_of_tau = range(len(velocities_all_atoms[0]) - t)
    values = [
        np.dot(velocities_this_atom[tau+t], velocities_this_atom[tau])
        for tau in range_of_tau
        for velocities_this_atom in velocities_all_atoms
    ]
    return np.mean(values)

def get_index_of_min_sem(velocity_autocorrelation_function, return_sem = False, stepsize_acc = 10):
    """
    Receives

    :param velocity_autocorrelation_function: the vaf as a list or numpy array
    :param return_sem: The boolean whether to return the standard error for each time

    As described above (?)
    we have to find a way to estimate the beginning of uncorrelated motion in a :term:`high-throughput` manner.
    To do this, the following algorithm is a applied:

    #.  Calculate the VAF for the trajectory for a time :math:`t_{max}` large enough (TODO: what is large enough?)
    #.  For each time :math:`t_{min} < t_{max}` calculate:

        .. math::
            {\cal{L}} (t)   &= \\frac{\int_{t}^{t_{max}} \\langle A^2 (t') \\rangle \\text{dt'}}{t_{max} - t}


    .. figure:: /images/estimate_beginning_uncorrelated_motion.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center

    .. figure:: /images/estimating_vaf_is_0_dependance.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center

    """
    length_of_vaf = len(velocity_autocorrelation_function)
    list_of_sem, t_min = zip(*[
                (
                    standard_error_of_mean(velocity_autocorrelation_function[starttime:]),
                    starttime
                )
                for starttime in range(0, int(0.8*length_of_vaf), stepsize_acc)
                ])
    index_of_min_sem = np.array(list_of_sem).argmin()
    if return_sem:
        return t_min[index_of_min_sem], list_of_sem
    return t_min[index_of_min_sem]

def nernst_einstein_eq(
        diffusion_mean, diffusion_std, diffusion_sem,
        temperature, nr_of_ions, volume, charge=1):
    """
    Calculate ionic conducitivity from the Nernst Einstein Equation :math:`\\bar{\\sigma} = \\frac{N e^2}{k_B T} \\bar{D}`
    Receives

    :param diffusion_mean: The diffusion in SI - units
    :param diffusion_std: The diffusion standard deviation in SI - units
    :param diffusion_sem: The diffusion standard error of the mean in SI - units
    :param temperature: Temperature of thermostat in K
    :param volume: The volume of the cell in cubic Angstrom
    :param charge: charge of the conducting ion in units of *e*, defaults to 1

    Returns

    :param conductivity_mean: Mean conductivity
    :param conductivity_std: Standard deviation of the conductivity
    :param conductivity_sem: Standard error of the mean of the conductivity

    The diffusion coefficient is calculated using the Einstein-Smulochowski equation :math:`D = \\frac{1}{6} \\overline{\\frac{d\\langle  \\text{MSD}(t)\\rangle}{\\text{dt}}}`
    This diffusion is inserted into the Nernst-Einstein equation above to obtain the conductivity.
    We can see the results in the following (plotted with :func:`carlo.scripts.plot_all_conductivities.plot_conductivities`

    .. figure:: /images/diffusion_coeffs_n_conductivities_top10_1.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center


    """
    density             =   nr_of_ions / volume # The density of ions per cubic angstrom!
    prefactor           =   charge**2 * e2*density* 1e30/(kB * temperature)  # also converting from A^3 -> m^3
    conductivity_mean   =   prefactor * diffusion_mean
    conductivity_std    =   prefactor * diffusion_std
    conductivity_sem    =   prefactor * diffusion_sem

    return conductivity_mean, conductivity_std, conductivity_sem



def trajectory_assessment(caller, *args, **kwargs):
    def maxwell_boltzmann(vel, mass):
        #~ print np.exp( - exp_factor* mass *vel**2 / T)
        #~ vel = vel_scaling * vel
        #~ alpha = atomic_mass * mass / (2* kB * T)
        #~ return  (alpha / np.pi )**(1.5) * 4* np.pi *  vel**2 * np.exp( - alpha *vel**2)
        return (prefactor * mass / T )**(1.5) * 4* np.pi *  vel**2 * np.exp( - exp_factor* mass *vel**2 / T)
    nr_of_bins = 100
    chunksize = 100
    traj =  args[0]['traj']['array']
    timestep_fs = args[0]['traj']['timestep_in_fs']
    struc =  args[1]['struc']

    atomic_mass = 1.66e-27
    kB = 1.3806488e-23
    vel_scaling = 1e5 / timestep_fs
    prefactor = atomic_mass / (2*np.pi*kB)
    exp_factor = atomic_mass  / (2 * kB)
    T = 600

    nr_of_atoms = traj.shape[1]
    trajlen = len(traj)
    velocities = [[vel_scaling*np.linalg.norm(traj[timestep][atomindex] - traj[timestep-1][atomindex])  for timestep in range(1, trajlen)] for atomindex in range(nr_of_atoms)]
    vel_dict = {atom:{'mass':atomic_masses[atomic_numbers[atom]], 'velocities':[], 'maxbol' : []} for atom in set(struc['atoms'])}
    [vel_dict[struc['atoms'][index]]['velocities'].append(vellist) for index,vellist in enumerate(velocities)]
    for key, d in vel_dict.items():
        d['velocities'] = zip(*d['velocities'])

    fig = plt.figure(figsize = (21,16))
    ax1 = fig.add_subplot(2,1,1)
    for key, d in vel_dict.items():
        temps = [np.mean([atomic_mass * d['mass'] * vel**2 / (3*kB) for vel in timestep]) for timestep in d['velocities']]
        ax1.plot(temps, label = key)

    plt.ylabel('Temperature')
    plt.xlabel('Timestep')
    plt.legend()

    ax2 = fig.add_subplot(2,1,2)

    for key, d in vel_dict.items():
        toplot = []
        for i in range(0, len(traj), chunksize):
            #~ fig = plt.figure()
            #~ plt.title(' Chunk: {}, Element {}'.format(int(i/chunksize), key))
            if chunksize > len(traj) -i: continue
            vels = flatten(d['velocities'][i:i+chunksize])
            x = np.arange(0, max(vels),max(vels)/nr_of_bins)

            counts, bins = np.histogram(vels, bins=nr_of_bins, range=(0,max(vels)), normed=True)
            half_bins = [(bins[jj] + bins[jj+1] ) /2.0  for jj in range(nr_of_bins)]
            boltzmann = [maxwell_boltzmann(vel, d['mass']) for vel in half_bins]
            #~ diffs = np.sum([(count - boltz)**2 for count, boltz in zip(counts, boltzmann)])
            diffs = np.std([count - boltz for count, boltz in zip(counts, boltzmann)])
            toplot.append([i, diffs])
        ax2.plot(*zip(*toplot), label = key)
    plt.xlabel('Timestep')
    plt.ylabel('Deviation from MB')
    plt.legend()
    plt.show()


def get_fixed_reference_msd(trajectory, structure, timestep_in_fs =  1, reference = 0):
    """
    Calculates the MSD with a fixed reference point
    (no running average)
    """
    traj = trajectory
    trajlen = len(traj)
    #~ tracklist = sorted([item for sublist in self.track_dict.values() for item in sublist])
    if type(reference) == int:
        initial_pos = traj[reference]
    elif reference == 'inputstructure':
        initial_pos = structure['positions']
    else: raise Exception('No valid reference point specified')

    msdcoeffs = [
        [
            [
                [
                    (timestep[j][k]-initial_pos[j][k])*(timestep[j][l]-initial_pos[j][l])
                    for k in range(3)
                ]
                for l in range(3)
            ]
            for j, position in enumerate(timestep)
        ]
        for timestep in trajectory
    ]
    msdisotrop = [
        [
            np.linalg.norm(initial_pos[j] - position)**2
            for j, position
            in enumerate(timestep)
        ]
        for timestep in trajectory
    ]


    msd_dict = {atom:{'msd':[]} for atom in set(structure['atoms'])}
    [
        msd_dict[atom]['msd'].append(zip(*msdisotrop)[index])
        for index, atom
        in enumerate(structure['atoms'])
    ]

    for atom, atomdict in msd_dict.items():
        atomdict['atom_mean'] = [np.mean(msd_at_timestep) for msd_at_timestep in  zip(*msd_dict[atom]['msd'])]


    plt.figure(figsize = (16,9)) #clear figures from before
    plt.xlabel('Time [fs]')
    plt.ylabel(r'MSD [$\AA^2$]')
    plt.title(r'MSD in ${}$'.format(''.join(['{}_{{{}}}'.format(atom, structure['atoms'].count(atom)) for atom in set(structure['atoms'])])) )
    times = [timestep_in_fs*i for i in range(len(trajectory))]
    for element, msd in msd_dict.items():
        plt.plot(times, msd['atom_mean'], linewidth = 4,color = jmol_colors[atomic_numbers[element]], label = 'MSD {}'.format( element))
        for msd_one_atom in msd['msd']:
            plt.plot(times, msd_one_atom, linestyle = '--', color = jmol_colors[atomic_numbers[element]], label = 'MSD {}'.format( element))

        #~ fit = self.diffusion_dict[element]
        #~ if len(element)> 3: continue #these are other keys in dictionary, like pdf file path
        #~ points = [fit['D']['slope']*x+fit['D']['intercept'] for x in range(self.equilibration_steps, len(self.traj))]
        #~ times = [1e15*self.dt*x for x in range(self.equilibration_steps, len(self.traj))]
        #~ plt.plot(times, points, linestyle = '--', color = jmol_colors[atomic_numbers[element]], label = 'Lin. Fit {}'.format(element))
    plt.show()
    return
    for key, indices in self.track_dict.items():
        msd_dict[key]  = {}
        diffusion_matrix = np.zeros((3,3))
        for coeff, i,j in [('D_xx',0,0), ('D_xy', 0,1), ('D_xz', 0,2), ('D_yy', 1,1), ('D_yz', 1,2), ('D_zz', 2,2)]:
            msd_dict[key][coeff] =  [np.mean([(timestep[index][i]-initial_pos[index][i])*(timestep[index][j]-initial_pos[index][j]) for index in indices]) for timestep in traj]
        msd_dict[key]['D'] = [np.mean([np.linalg.norm(initial_pos[index] - timestep[index])**2 for index in indices]) for timestep in traj]
    self.log.write('   Computing MSD and writing results to {}\n'.format(msd_file_path))
    print 11
    json.dump(msd_dict, open(msd_file_path, 'w'))
    print 12
    print 1
    if os.path.exists(res_file_path) and not self.overwrite:
        print 2
        diffusion_dict = json.load(open(res_file_path))
        self.log.write('   Loading linear fits from {}\n'.format(res_file_path))
    else:
        print 3
        self.log.write('   Computing linear fits and storing results in {}\n'.format(res_file_path))
        diffusion_dict = {}
        for element, msd_element_dict in msd_dict.items():
            #~ print element
            diffusion_dict[element] = {}
            for coefficient_key, msd in msd_element_dict.items():
                diffusion_dict[element][coefficient_key] = {
                    key:val
                    for key, val in zip(
                        ['slope', 'intercept', 'r-value', 'p-value', 'stderr'],
                        linregress(
                            range(self.equilibration_steps, trajlen),
                            msd[self.equilibration_steps:]
                        )
                    )
                }
                if coefficient_key == 'D':
                    diffusion_dict[element][coefficient_key]['value'] = 1e-20 / self.dt * diffusion_dict[element][coefficient_key]['slope'] / 6
                else:
                    diffusion_dict[element][coefficient_key]['value'] = 1e-20 / self.dt * diffusion_dict[element][coefficient_key]['slope'] / 2
            diffusion_dict[element]['diffusion_matrix'] = [
                [
                    diffusion_dict[element]['D_'+''.join(sorted([dir1,dir2]))]['value']
                    for dir1 in ('x','y','z')
                ]
                for dir2 in ('x','y','z')
            ]

        cell = self.struc['cell']
        volume = np.dot(cell[0], np.cross(cell[1], cell[2]))*10**(-30)  #cell volume in cubic meters
        #~ print volume
        #~ print Atoms(cell = cell).get_volume() #just a test
        nr_of_ions = self.struc['atoms'].count('Li')
        e = 1.60217657*10**(-19)
        e2 = e**2
        #calculating the conductivity sigma of Li
        for element in diffusion_dict.keys():
            nr_of_ions = self.struc['atoms'].count(element)
            diff = np.array(diffusion_dict[element]['diffusion_matrix'])
            sigma = e2*diff*nr_of_ions/(volume*1.3806488*10**(-23) * self.temperature)
            diffusion_dict[element]['sigma']  =  sigma.tolist()
            diffusion_dict[element]['sigma_slope']  =  e2*diffusion_dict[element]['D']['value']*nr_of_ions/(volume*1.3806488*10**(-23) * self.temperature)
        self.log.write('\n   conductivity for Li is {} S/m\n'.format(diffusion_dict['Li']['sigma_slope']))
        json.dump(diffusion_dict, open(res_file_path, 'w'))
    self.diffusion_dict = diffusion_dict
    self.msd_dict = msd_dict


def get_rdf(self, filename, nr_of_bins=200, nr_of_timesteps = False, timelags =  range(0, 201, 20)):
    """
    Calculate the radial distribution function in a simulation"""

    def get_distance(point1, point2):
        point1 =   collapse_into_unit_cell(point1, self.cell)
        point2 =   collapse_into_unit_cell(point2, self.cell)
        return find_closest_periodic_image(point2, point1, self.cell)[0]
    def get_density(distance, counts):
        #print counts, volume, nr_of_timesteps, nr_of_lithiums, nr_of_atoms, distance,  binwidth
        return counts * volume / (nr_of_timesteps * nr_of_lithiums* nr_of_atoms * 4*np.pi * distance**2 *binwidth)
        #~ return counts * volume / (nr_of_timesteps * nr_of_lithiums* nr_of_atoms * 4.0  / 3.0*np.pi * ((distance+binwidth)**3 -distance**3))

    self.timelags_dt = [int(float(timelag)/self.timestep_in_fs) for timelag in timelags] #timelag in fs ->timelag in timesteps
    self.timelags_fs = timelags
    #~ print self.timelags_dt
    #~ print self.timelags_fs
    maxcorrect = 0.5*min([np.linalg.norm(latticevector) for latticevector in self.struc['cell']]) #this is how far you can expand the radius if you still want correct result
    binwidth = float(maxcorrect)/nr_of_bins
    nr_of_lithiums = self.struc['atoms'].count('Li')
    cell = self.struc['cell']
    volume = np.dot(cell[0], np.cross(cell[1], cell[2]))  #cell volume in cubic meters
    #~ end = len(self.traj)
    start  = self.equilibration_steps

    if nr_of_timesteps:
        end = nr_of_timesteps +start
    else:
        #~ start = self.equilibration_steps
        #~ nr_of_timesteps = end - start
        end =  min([start+int(3000.0/self.timestep_in_fs) , len(self.traj)])   #default take first 3ps after equilibration
        nr_of_timesteps = end-start
    print start, end
    rdf_dict = {}
    coordination_dict = {}
    #~ print range(start, end)
    self.log.write('   Calculating RDF for elements {} with respect to Li\n'.format(','.join(self.track_dict.keys())))


    for element in self.track_dict.keys():
        nr_of_atoms = self.struc['atoms'].count(element)
        #~ print element, nr_of_atoms
        fil = filename.replace('.npy', '_{}.npy'.format(element))
        if os.path.exists(fil) and not self.overwrite:
            self.log.write('      Reading distances from {} \n'.format(fil))
            rdf_dict[element] = np.load(fil)
        else:
            self.log.write('      Calculating distances for {} \n'.format(element))
            #~ distances = [[np.linalg.norm(np.array(collapse_into_unit_cell(self.traj[timestep-timelag][li_index], self.cell)) - collapse_into_unit_cell(self.traj[timestep][el_index], self.cell)) \
                            #~ for li_index in self.track_dict['Li']  \
                            #~ for el_index in self.track_dict[element] \
                            #~ for timestep in range(self.equilibration_steps, len(self.traj)) \
                            #~ if li_index != el_index] for timelag in self.timelags]
            distances = [[get_distance(self.traj[timestep-timelag][li_index], self.traj[timestep][el_index]) \
                            for li_index in self.track_dict['Li']  \
                            for el_index in self.track_dict[element] \
                            for timestep in range(start, end) \
                            if li_index != el_index] \
                            for timelag in self.timelags_dt]
            #~ histos  = [np.histogram(distance, bins =10000, normed =nr_of_atoms) for distance in distances]
            histos  = [np.histogram(distance, bins = nr_of_bins, range = (0, maxcorrect)) for distance in distances]
            rdfs = [[(distance, get_density(distance, counts)) for counts, distance in zip(*histo)[1:]] for histo in histos]
            #~ n_ = [[(distance, np.sum(rdf[:distance_index])) for distance_index in range(nr_of_bins)] for distance,
            self.log.write('      Saving histogram to {} \n'.format(fil))
            np.save(fil, rdfs)
            rdf_dict[element] = rdfs

    self.rdf_dict = rdf_dict

def get_mean_msd_of_block(args):
    block_start, range_for_t, zipped_trajectories, timestep_in_fs, index_start_vaf_is_0, block_length = args
    msd_isotrop_this_block = [
        np.mean(
            [
                np.linalg.norm(trajectory_of_ion[tau + t] - trajectory_of_ion[tau])**2
                for tau in np.arange(block_start, block_start + block_length)
                for trajectory_of_ion in zipped_trajectories
            ]
        )
        for t in range_for_t
    ]

    slope_this_block, intercept_this_block, r_value, p_value, std_err = linregress(
        timestep_in_fs * range_for_t[index_start_vaf_is_0:],
        msd_isotrop_this_block[index_start_vaf_is_0:]
    )
    return slope_this_block, intercept_this_block, msd_isotrop_this_block




def calc_cell_volume(cell):
    """
    Calculates the volume of a cell given the three lattice vectors.

    It is calculated as cell[0] . (cell[1] x cell[2]), where . represents
    a dot product and x a cross product.

    :param cell: the cell vectors; the must be a 3x3 list of lists of floats,
            no other checks are done.

    :returns: the cell volume.
    """
    # returns the volume of the primitive cell: |a1.(a2xa3)|
    a1 = cell[0]
    a2 = cell[1]
    a3 = cell[2]
    a_mid_0 = a2[1] * a3[2] - a2[2] * a3[1]
    a_mid_1 = a2[2] * a3[0] - a2[0] * a3[2]
    a_mid_2 = a2[0] * a3[1] - a2[1] * a3[0]
    return abs(a1[0] * a_mid_0 + a1[1] * a_mid_1 + a1[2] * a_mid_2)




if __name__ == '__main__':
    from aiida.backends.utils import load_dbenv, is_dbenv_loaded
    if not is_dbenv_loaded():
        load_dbenv()
    from argparse import ArgumentParser
    import os
    #~ from aiida.tools.codespecific.quantumespresso.pwinputparser import PwInputFile
    from aiida.tools.codespecific.quantumespresso.cpinputparser import CpInputFile
    parser = ArgumentParser()
    parser.add_argument('-i', '--input',help='CP or PW inputfile',type=str, required=True)
    parser.add_argument('-p', '--positions', help='The file storing positions', required=True)
    parser.add_argument('-v', '--velocities', help='The file storing velocities', required=True)
    parser.add_argument('--pos-units', type=str, choices=('angstrom', 'atomic', 'bohr'), required=True)
    parser.add_argument('--vel-units', type=str, choices=('cp', 'pw', 'a_p_fs', 'lammps_electron'), required=True)
    parser.add_argument('--calculation', help='Type of calculation. If not specified, I will guess from input', choices=('cp', 'pw'))
    parser.add_argument('-a', '--atoms-of-interest', help='Atoms of interest, default=["Li"]', nargs='+',default=["Li"])
    parser.add_argument('--recenter', help='recenter', action='store_true')
    parser.add_argument('--discard', help='discard this much (in fs)', type=float)
    parser.add_argument('--kinetic-energies', help='calculate kinetic energies', action='store_true')
    parser.add_argument('--is-boltzmann-distributed', help='calculate kinetic energies', action='store_true')
    parser.add_argument('--format', help='The format the data is stored', choices=('xyz','axyz'), default='xyz')
    #~ parser.add_argument('--kinetic-energies', help='calculate kinetic energies', action='store_true')
    parsed_args = parser.parse_args(sys.argv[1:])



    inpfile = CpInputFile(os.path.abspath(parsed_args.input)) # Taking that because it doesnt complain about KPOINTS missing
    s = inpfile.get_structuredata()
    n = inpfile.namelists

    if parsed_args.calculation == 'cp':
        calc_type='cp'
    elif parsed_args.calculation == 'pw':
        calc_type='pw'
    elif n['CONTROL']['calculation'] =='cp':
        calc_type='cp'
    elif n['CONTROL']['calculation'] =='md':
        calc_type='pw'
    else:
        raise Exception("Cannot determine type of calculation")

    if calc_type=='cp':
        timestep_fs = n['CONTROL']['dt']*n['CONTROL'].get('iprint', 1)*timeau_to_sec*1e15
    else:
        timestep_fs = n['CONTROL']['dt']*n['CONTROL'].get('iprint', 1)*timeau_to_sec*1e15*2


    t = TrajectoryAnalyzer()

    t.set_structure(s, species_of_interest=parsed_args.atoms_of_interest)

    t.read_trajectories(pos_files=[parsed_args.positions], vel_files=[parsed_args.velocities],
            timestep_in_fs=timestep_fs, recenter=parsed_args.recenter, pos_units=parsed_args.pos_units,
            vel_units=parsed_args.vel_units, discard_fs=parsed_args.discard, format=parsed_args.format)
    if parsed_args.kinetic_energies:
        t.calculate_kinE(1)
    if parsed_args.is_boltzmann_distributed:
        t.is_boltzmann_distributed(n['IONS']['tempw'])

    t.plot_results()

