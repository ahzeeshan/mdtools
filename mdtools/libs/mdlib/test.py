from trajectory_analysis import TrajectoryAnalyzer

import numpy as np
from ase import Atoms, Atom
# Creating the trajectory of a free particle moving with velocity 1 in x difrection


nat = 20
nstep = 1000000
target_temperature = 500.

def write():


    positions = np.zeros((nstep, nat, 3))
    velocities = np.zeros((nstep, nat, 3))



    def get_forces(position):
        #~ print np.random.random()
        forces = 3*np.sin(0.01*position)+10*np.sin(0.03*position) + np.random.normal(scale=0.18, size=(nat, 3))
        #~ forces[:, 1] = 0.
        #~ forces[:, 2] = 0.
        return forces


    last_pos = np.zeros((nat,3))
    last_vel = np.zeros((nat,3))
    last_forces = np.zeros((nat,3))


    for i in xrange(0, nstep):
        # Perform a verlocity verlet integration step with random forces:
        new_pos = last_pos + last_vel + 0.5*last_forces
        new_forces = get_forces(new_pos)
        #~ print new_forces
        new_vel = last_vel + 0.5*(last_forces + new_forces)
        for iat in range(nat):
            new_vel[iat] = np.sqrt(3. * target_temperature / np.sum(new_vel[iat]**2)) * new_vel[iat]  # .5 m v**2 = 3/2 kB T with m=kB=1
            #~ print np.sum(new_vel[iat]**2)
        positions[i] = new_pos
        velocities[i] = new_vel
        last_vel = new_vel
        last_pos = new_pos
        last_forces=new_forces

    np.save('positions', positions)
    np.save('velocities', velocities)

def read():
    positions = np.load('positions.npy')
    velocities = np.load('velocities.npy')
    atoms = Atoms()

    for iat in range(nat):
        atoms.append(Atom('H', [0,0,0]))

    ta = TrajectoryAnalyzer()
    ta.set_structure(atoms)
    ta.set_trajectories([positions], velocities=[velocities], timestep_in_fs=1, recenter=False)

    if 1:
        ta.get_vaf(
                t_start_fit_fs=300.000,
                t_end_fit_fs=600.00,
                nr_of_blocks=10,
                stepsize_tau=1,
                integration='trapezoid'
            )
        ta.velocities_fourier()
    if 1:

        ta.get_msd(
                t_start_fit_fs=300.00,
                t_end_fit_fs=600.00,
                nr_of_blocks=10,
                stepsize_inner=1,
             
            )
    ta.plot_results(
            label_blocks=False,
            #~ plot_trajectory=True
        )

#~ write()
read()
